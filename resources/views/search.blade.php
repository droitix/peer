@extends('layouts.v2applist')

@section('content')

@include('includes.v2bannersearch')
<main>



<div class="container margin_60_35">
            
            <div class="row">
                @if ($listings->count())
        @foreach ($listings as $listing)
                  @include ('listings.partials._listing', compact('listing'))
                @endforeach

            </div>
            <!-- /row -->
            {{ $listings->links() }}
            

             @else
                 <span>Your search did not return any results</span>
                 @endif
            
        </div>
        <!-- /container -->
</main>

</body>
</html>

@endsection
