
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="zxx">
<!--<![endif]-->

<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-136070512-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-136070512-1');
</script>


    <!-- Basic metas
    ======================================== -->
    <meta charset="utf-8">
    <meta name="author" content="Axilweb">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!-- Mobile specific metas
    ======================================== -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Page Title
    ======================================== -->
    <title>Platinum Coin Auction | Coin Auction Network</title>

    <!-- links for favicon
    ======================================== -->
    <link rel="shortcut icon" href="/images/purplefavicon.ico" type="image/x-icon">
    <link rel="icon" href="/images/purplefavicon.ico" type="image/x-icon">

    <!-- Icon fonts
    ======================================== -->
    <link rel="stylesheet" type="text/css" href="/homepage/css/cryptowallet-argon.css">

    <!-- css links
    ======================================== -->
    <!-- Bootstrap link -->
    <link rel="stylesheet" type="text/css" href="/homepage/css/bootstrap.min.css">

    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="/homepage/css/slick.css">
    <link rel="stylesheet" type="text/css" href="/homepage/css/slick-theme.css">

    <!-- Magnific popup -->
    <link rel="stylesheet" type="text/css" href="/homepage/css/magnific-popup.css">

    <!-- Custom styles -->
    <link rel="stylesheet" type="text/css" href="/homepage/css/main.css">

    <!-- Responsive styling -->
    <link rel="stylesheet" type="text/css" href="/homepage/css/responsive.css">
</head>


<!-- body starts
============================================ -->

<body>

    <!-- Preloader -->
    <div class="o-preloader">
        <div class="o-preloader_inner"></div>
    </div>


    <div class="radial-bg radial-bg__banner">
    </div>
    <!-- End of .radial-bg -->

    <!-- navbar starts
============================================ -->
    <nav class="navbar navbar-expand-lg">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="/images/whitelogo.png" style="width: 200px;" alt="brand logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                <span class="toggler-icon-bar bar1"></span>
                <span class="toggler-icon-bar bar2"></span>
                <span class="toggler-icon-bar bar3"></span>
            </button>
            <!-- End of .navbar-toggler -->

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/terms') }}">About & Terms
                        </a>
                    </li>

                    <li class="nav-item">
                         @guest
                        <a class="nav-link" href="{{ url('/register') }}">Register
                        </a>
                        @else
                         <a class="nav-link" href="#">Hi, {{ Auth::user()->name }}
                        </a>
                        @endguest
                    </li>
                    <li class="nav-item">
                         @guest
                        <a class="btn btn-small" href="{{ url('/login') }}">Login</a>
                         @else
                         <a class="btn btn-small" href="{{ url('/dashboard') }}">Dashboard</a>
                         @endguest
                    </li>
                </ul>
            </div>
        </div>
        <!-- End of .container -->
    </nav>
    <!-- End of .navbar -->

    <!-- banner starts
============================================ -->
    <section class="banner">

        <div class="container">
            <div class="banner-wrapper d-flex align-items-center">
                <div class="" >

                    <h1>Multiply Funds In Days
                        <br>Earn Over 110% interest every 2 weeks</h1>
                    <p>Start with a minimum of R100 and Participate in the great coin auctions.
                    </p>
                    @guest
                    <a class="btn" href="{{ url('/register') }}">Create Account</a>
                    @else
                     <a class="btn" href="{{ route('listings.create', [$area]) }}">Start Today</a>
                    @endguest
                </div>
                <!-- End of .banner-txt -->

                <div class="img-container img-container__banner" >
                    <img src="/images/blackshot.png" id="optionalstuff" alt="home banner image" class="img-fluid ml-auto">
                </div>
                <!-- End of .img-container -->
            </div>
            <!-- End of .banner-wrapper -->
        </div>
        <!-- End of .container -->
    </section>
    <!-- End of .banner -->

     <!-- roadmap-to-success starts
============================================ -->
    <section class="section">
        <div class="radial-bg radial-bg__roadmap">
        </div>
        <!-- End of .radial-bg -->

        <div class="container">
            <div class="roadmap-to-success section-padding" id="roadmap">
                <div class="section-heading text-center">
                    <h2>Buy Coins Today And Sell At a Higher Price</h2>
                    <p>Platinum coins are always on demand and we have the highest sale rates in South Africa </p>
                </div>
                <!-- End of .section-heading -->
            </div>
            <!-- End of .roadmap-to-success -->
        </div>
        <!-- End of .container -->

        <div class="roadmap-container">

            <div class="row align-items-end no-gutters">
                <div class="col-md roadmap-label l-1" title="Click to open">
                    <div class="text-content">30% in 2 Days
                        <span>R1000 becomes R1300</span>
                    </div>
                    <div class="data">
                        <div class="line"></div>

                    </div>
                    <!-- End of .data -->
                </div>
                <!-- End of .roadmap-label -->

                <div class="col-md roadmap-label l-2" title="Click to open">

                    <div class="data">
                        <div class="line"></div>

                    </div>
                    <!-- End of .data -->
                </div>
                <!-- End of .roadmap-label -->

                <div class="col-md roadmap-label l-3" title="Click to open">
                    <div class="text-content">
                        50% in 7days
                        <span>R1000 becomes R1500</span>
                    </div>
                    <div class="data">
                        <div class="line"></div>

                    </div>
                    <!-- End of .data -->
                </div>
                <!-- End of .roadmap-label -->

                <div class="col-md roadmap-label l-4" title="Click to open">


                    <div class="data">
                        <div class="line"></div>


                    </div>
                    <!-- End of .data -->
                </div>
                <!-- End of .roadmap-label -->

                <div class="col-md roadmap-label l-5" title="Click to open">
                    <div class="text-content">
                        100% in 15 Days
                        <span>R1000 becomes R2000</span>
                    </div>
                    <div class="data">
                        <div class="line"></div>

                    </div>
                    <!-- End of .data -->
                </div>
                <!-- End of .roadmap-label -->

                <div class="col-md roadmap-label l-6" title="Click to open">

                    <div class="data">
                        <div class="line"></div>

                    </div>
                    <!-- End of .data -->
                </div>
                <!-- End of .roadmap-label -->

                <div class="col-md roadmap-label l-7" title="Click to open">
                    <div class="text-content">
                        200% in 30 Days
                        <span>R1000 becomes R3000</span>
                    </div>

                    <div class="data">
                        <div class="line"></div>

                    </div>
                    <!-- End of .data -->
                </div>
                <!-- End of .roadmap-label -->
            </div>
            <!-- End of .row -->

            <svg class="roadmap-line" width="1920">
                <defs>
                    <linearGradient id="linear-gradient" y1="42.22" x2="1920.75" y2="42.22" gradientTransform="matrix(1, 0, 0, -1, 0, 82)" gradientUnits="userSpaceOnUse">
                        <stop offset="0" stop-color="#3023ae" />
                        <stop offset="1" stop-color="#c86dd7" />
                    </linearGradient>
                </defs>
                <path class="roadmap-line-path" d="M -290 0 l2500 0" />
            </svg>
        </div>
        <!-- End of .roadmap-container -->
    </section>
    <!-- End of .roadmap-to-success -->


    <!-- features starts
============================================ -->
    <section class="features">
        <div class="radial-bg radial-bg__features">
        </div>
        <!-- End of .radial-bg -->

        <div class="container">
            <div class="feature-wrapper">
                <div class="feature-content section-padding">
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <div class="text-content">
                                <h2>Take advantage of our Huge Refferal Bonus
                                <p>Reffer anyone to create an account using your refferal email address and get 15% of every deposit they will make, they will in turn get loan credit points for using your refferal email</p>



                            </div>
                            <!-- End of .text-content -->
                        </div>
                        <!-- End of .col-md-6 -->

                        <div class="col-md-6">
                            <div class="img-container">
                                <img src="/images/blackshot.png" alt="feature image">
                            </div>
                            <!-- End of .img-container -->
                        </div>
                        <!-- End of .col-md-6 -->
                    </div>
                    <!-- End of .row -->
                </div>
                <!-- End of .feature-content -->



    <!-- benefits-of-solution starts
============================================ -->
    <section class="section">
        <div class="container">
            <div class="benefits-of-solution section-padding">
                <div class="section-heading text-center">
                    <h2>Benefits of the Platinum coin Auction </h2>
                    <p>We have thousands of South Africans Participating and heavily invested , ready to jump on every oportunity.</p>
                </div>
                <!-- End of .section-heading -->

                <div class="row">
                    <div class="col-md-6">
                        <div class="benefits-content">

                            <h4>Real Time Accounts</h4>
                            <p>By creating an account you get to participate in the auction in real-time and monitor all transactions</p>
                        </div>
                        <!-- End of inner-content -->
                    </div>
                    <!-- End of .col-md-6 -->

                    <div class="col-md-6">
                        <div class="benefits-content">

                            <h4>Investment Control</h4>
                            <p>You get to control your investment portfolio in different circles and varying interest rates.</p>
                        </div>
                        <!-- End of inner-content -->
                    </div>
                    <!-- End of .col-md-6 -->

                    <div class="col-md-6">
                        <div class="benefits-content">

                            <h4>Easy Crowdfunding</h4>
                            <p>Harness the power of crowdfunding, by taking advantage of our huge investor base and take your product to the next level.</p>
                        </div>
                        <!-- End of inner-content -->
                    </div>
                    <!-- End of .col-md-6 -->

                    <div class="col-md-6">
                        <div class="benefits-content">

                            <h4>Convenience</h4>
                            <p>No verifications are required and the account creation process is smooth</p>
                        </div>
                        <!-- End of inner-content -->
                    </div>
                    <!-- End of .col-md-6 -->


                </div>
                <!-- End of .row -->
            </div>
            <!-- End of .benefits-of-solution -->
        </div>
        <!-- End of .container -->
    </section>
    <!-- End of section -->


    <!-- footer starts
============================================ -->
    <footer class="footer">
        <div class="radial-bg radial-bg__footer">
        </div>
        <!-- End of .radial-bg -->
        <div class="container">
            <div class="page-footer">
                <div class="footer-menu">
                    <div class="row">
                        <div class="col-md">
                            <a href="index.html" class="d-block">
                                <img src="/images/whitelogo.png" style="width: 150px;" alt="footer logo">
                            </a>
                        </div>
                        <!-- End of .col -->



                        <div class="col-md">
                            <h5>Help</h5>
                            <ul class="footer-nav">
                                <li>
                                    <a href="{{ url('/terms') }}">Terms &amp; Conditions</a>
                                </li>
                                <li>
                                    <a href="{{ url('/terms') }}">Privacy Policy</a>
                                </li>

                            </ul>
                            <!-- End of .footer-nav -->
                        </div>
                        <!-- End of .col -->

                        <div class="col-md-4">
                            <h5>Social Links</h5>
                            <p>Come and chat with us.</p>


                            <div class="social-icons">
                                <a href="#" target="_blank" rel="noopener">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                                <a href="#" target="_blank" rel="noopener">
                                    <i class="fab fa-twitter"></i>
                                </a>
                                <a href="#" target="_blank" rel="noopener">
                                    <i class="fab fa-instagram"></i>
                                </a>
                                <a href="#" target="_blank" rel="noopener">
                                    <i class="fab fa-youtube"></i>
                                </a>
                                <a href="#" target="_blank" rel="noopener">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                                <a href="#" target="_blank" rel="noopener">
                                    <i class="fab fa-pinterest-p"></i>
                                </a>
                            </div>
                            <!-- End of .social-icons -->
                        </div>
                        <!-- End of .col -->
                    </div>
                    <!-- End of .row -->
                </div>
                <!-- End of .footer-menu -->

                <div class="footer-bottom">
                    <p>Copyright &copy; 2020. All rights reserved by
                        <a href="#">Platinum Coin</a>
                    </p>
                    <div class="dropdown text-xs-right ml-auto">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Language:
                            <span>English</span>
                            <i class="fa fa-angle-up"></i>
                        </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">English ZA</a>

                        </div>
                        <!-- End of .dropdown-menu -->
                    </div>
                    <!-- End of .dropdown -->
                </div>
                <!-- End of .footer-copyright -->
            </div>
            <!-- End of .page-footer -->
        </div>
        <!-- End of .container -->
    </footer>
    <!-- End of page-footer -->


    <!-- Javascripts
    ======================================= -->
    <!-- jQuery -->
    <script src="/homepage/js/jquery-3.2.1.min.js"></script>
    <script src="/homepage/js/jquery-migrate.min.js"></script>
    <!-- Bootstrap js -->
    <script src="/homepage/js/bootstrap.bundle.min.js"></script>
    <!-- font awesome -->
    <script src="/homepage/js/fontawesome-all.min.js"></script>
    <!-- jQuery Countdown plugin -->
    <script src="/homepage/js/jquery.countdown.min.js"></script>
    <!-- jQuery Easing Plugin -->
    <script src="/homepage/js/easing-1.3.js"></script>
    <!-- jQuery progressbar plugin -->
    <script src="/homepage/js/jquery.waypoints.min.js"></script>
    <!-- Bootstrap Progressbar -->
    <script src="/homepage/js/bootstrap-progressbar.min.js"></script>
    <!-- ImagesLoaded js -->
    <script src="/homepage/js/imagesloaded.pkgd.min.js"></script>
    <!-- Slick carousel js -->
    <script src="/homepage/js/slick.min.js"></script>
    <!-- Magnific popup -->
    <script src="/homepage/js/jquery.magnific-popup.min.js"></script>

    <!-- pie chart -->
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <!-- Custom js -->
    <script src="/homepage/js/main.js"></script>


      <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5c870fb6101df77a8be212cb/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->


</body>

</html>
