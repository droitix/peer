@extends('layouts.app')

@section('content')
 
     <main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h6 class="page-title-heading mr-0 mr-r-5">Admin</h6>
                    <p class="page-title-description mr-0 d-none d-md-inline-block"> </div>
                <!-- /.page-title-left -->
                <div class="page-title-right d-none d-sm-inline-flex">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active">Impersonate</li>
                    </ol>
                </div>
                <!-- /.page-title-right -->
            </div>
            <!-- /.page-title -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="widget-list">
                <div class="row">
                    <div class="col-md-12 widget-holder">
                        <div class="widget-bg">
                            <div class="widget-body clearfix">
                                <h5 class="box-title mr-b-0">Admin</h5>
                                <p class="text-muted"></p>

                                  <form action="{{ route('admin.impersonate') }}" method="POST">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="l8">Impersonate</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <input class="form-control" name="email" id="email" placeholder="Email address" type="text"> 
                                               <button type="submit" class="btn btn-dark">Submit</button>
                                            </div>
                                        </div>
                                    </div> 

                                     {{ csrf_field() }}
                                  </form> 
          
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.widget-list -->
        </main>
        <!-- /.main-wrapper -->
 
@endsection
