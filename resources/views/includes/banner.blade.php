<div class="tr-banner section-before bg-image" background="/2.0/img/banner.jpeg">
        <div class="container">
            <div class="banner-content">
                <h1>List your business here in {{ $area->name }}</h1>
                <h2>Let people around you know what you are doing!</h2>
                
                  @include('listings.partials._search')           
            </div><!--/. banner-content -->
        </div><!-- /.container -->
    </div><!-- /.tr-banner -->
