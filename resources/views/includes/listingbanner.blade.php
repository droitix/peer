
    <div class="tr-banner section-before bg-image">
        <div class="container">
            <div class="banner-content">
                <h1>You are browsing {{ $category->name }} in {{ $area->name }}</h1>
                <h2>Let people in {{ $area->name }} and all zimbabwe know what you are doing</h2>
                
                  @include('listings.partials._search')           
            </div><!--/. banner-content -->
        </div><!-- /.container -->
    </div><!-- /.tr-banner -->