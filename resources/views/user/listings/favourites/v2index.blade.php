@extends('layouts.v2adminapp')


@section('content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">favourites</li>
      </ol>
        <div class="box_general">
            <div class="header_box">
                <h2 class="d-inline-block">My favourites list</h2>
               
            </div>
     <div class="list_general">
    @if ($listings->count())
        @each ('listings.partials.v2base_favourites', $listings, 'listing')
        {{ $listings->links() }}
    @else
        <p>No  listings.</p>
    @endif
            
</div>
       
        <!-- /pagination-->
      </div>
      <!-- /container-fluid-->
    </div>
    <!-- /container-wrapper-->



@endsection