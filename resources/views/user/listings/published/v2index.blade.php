@extends('layouts.v2adminapp')


@section('content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Bookings list</li>
      </ol>
        <div class="box_general">
            <div class="header_box">
                <h2 class="d-inline-block">My listings list</h2>
               
            </div>
     <div class="list_general">
    @if ($listings->count())
        @each ('listings.partials.v2base_published', $listings, 'listing')
        {{ $listings->links() }}
    @else
        <p>No  listings.</p>
    @endif
            
</div>
        <nav aria-label="...">
            <ul class="pagination pagination-sm add_bottom_30">
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#">Next</a>
                </li>
            </ul>
        </nav>
        <!-- /pagination-->
      </div>
      <!-- /container-fluid-->
    </div>
    <!-- /container-wrapper-->



@endsection