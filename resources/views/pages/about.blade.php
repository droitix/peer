@extends('layouts.v2applist')

@section('content')
   <div class="sub_header_in sticky_header">
        <div class="container">
            <h1>About Ndepapi Listing Engine</h1>
        </div>
        <!-- /container -->
    </div>
    <!-- /sub_header -->
    
    <main>
        <div class="container margin_80_55">
            <div class="main_title_2">
                <span><em></em></span>
                <h2>Why Choose Ndepapi</h2>
                <p>We are a location specific listing engine and we are quality driven.</p>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <a class="box_feat" href="#0">
                        <i class="fas fa-layer-group"></i>
                        <h3>+ 120 Categories</h3>
                        <p>We are proud to host over 120 categories in all sectors of the economy in Zimbabwe.This ensures that you will find your business a spot.</p>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6">
                    <a class="box_feat" href="#0">
                        <i class="fas fa-headset"></i>
                        <h3>All Round Support</h3>
                        <p>Our support is all round and we are there to assist you in any query or business development agenda. </p>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6">
                    <a class="box_feat" href="#0">
                        <i class="fas fa-thumbtack"></i>
                        <h3>+ 700 Locations in Zimbabwe</h3>
                        <p>Location is an important factor in business marketing and we have taken care of that by listing almost every location in the land of Zimbabwe.</p>
                    </a>
                </div>
               
            </div>
            <!--/row-->
        </div>
        <!-- /container -->

        <div class="bg_color_1">
            <div class="container margin_80_55">
                <div class="main_title_2">
                    <span><em></em></span>
                    <h2>Our Vision</h2>
                    <p>We are targeting to list everything in Zimbabwe by 2020.</p>
                </div>
                <div class="row justify-content-between">
                    <div class="col-lg-3 wow" data-wow-offset="150">
                        <figure class="block-reveal">
                            <div class="block-horizzontal"></div>
                            <img src="/svg/paul.png" class="img-fluid" alt="">
                        </figure>
                    </div>
                    <div class="col-lg-9">
                        <p>Te plan is to make sure that the whole of zimbabwe is listed, for purposes of discoverability by both external investors and our local population. This is going to be possible through this platform and we are dedicated to see to it that this comes to pass.</p>
                        <p>A connected nation is a very powerfull one</p>
                        <p><em>CTO Paul Kumbweya </em></p>
                    </div>
                </div>
                <!--/row-->
            </div>
            <!--/container-->
        </div>
        <!--/bg_color_1-->

        <div class="container margin_80_55">
            <div class="main_title_2">
                <span><em></em></span>
                <h2>Our Team</h2>
                <p>This is the  team that is working on ground breaking technology here.</p>
            </div>
            <div id="carousel" class="owl-carousel owl-theme">
                <div class="item">
                    <a href="#0">
                        <div class="title">
                            <h4>Paul Kumbweya<em>Founder and CTO</em></h4>
                        </div><img src="/svg/paul.jpg" alt="">
                    </a>
                </div>
                <div class="item">
                    <a href="#0">
                        <div class="title">
                            <h4>Lucas Smith<em>Marketing</em></h4>
                        </div><img src="img/2_carousel.jpg" alt="">
                    </a>
                </div>
                <div class="item">
                    <a href="#0">
                        <div class="title">
                            <h4>Paul Stephens<em>Business strategist</em></h4>
                        </div><img src="img/3_carousel.jpg" alt="">
                    </a>
                </div>
                <div class="item">
                    <a href="#0">
                        <div class="title">
                            <h4>Pablo Himenez<em>Customer Service</em></h4>
                        </div><img src="img/4_carousel.jpg" alt="">
                    </a>
                </div>
                <div class="item">
                    <a href="#0">
                        <div class="title">
                            <h4>Andrew Stuttgart<em>Admissions</em></h4>
                        </div><img src="img/5_carousel.jpg" alt="">
                    </a>
                </div>
            </div>
            <!-- /carousel -->
        </div>
        <!--/container-->
    </main>
    <!--/main-->
@endsection
