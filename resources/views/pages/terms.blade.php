
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="zxx">
<!--<![endif]-->

<head>

    <!-- Basic metas
    ======================================== -->
    <meta charset="utf-8">
    <meta name="author" content="Axilweb">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!-- Mobile specific metas
    ======================================== -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Page Title
    ======================================== -->
    <title>Streetwage | Terms</title>

    <!-- links for favicon
    ======================================== -->
    <link rel="shortcut icon" href="/images/purplefavicon.ico" type="image/x-icon">
    <link rel="icon" href="icon/favicon.ico" type="image/x-icon">

    <!-- Icon fonts
    ======================================== -->
    <link rel="stylesheet" type="text/css" href="/homepage/css/cryptowallet-argon.css">

    <!-- css links
    ======================================== -->
    <!-- Bootstrap link -->
    <link rel="stylesheet" type="text/css" href="/homepage/css/bootstrap.min.css">

    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="/homepage/css/slick.css">
    <link rel="stylesheet" type="text/css" href="/homepage/css/slick-theme.css">

    <!-- Magnific popup -->
    <link rel="stylesheet" type="text/css" href="/homepage/css/magnific-popup.css">

    <!-- Custom styles -->
    <link rel="stylesheet" type="text/css" href="/homepage/css/main.css">

    <!-- Responsive styling -->
    <link rel="stylesheet" type="text/css" href="/homepage/css/responsive.css">
</head>


<!-- body starts
============================================ -->

<body>

    <!-- Preloader -->


    <div class="radial-bg radial-bg__banner">
    </div>
    <!-- End of .radial-bg -->

    <!-- navbar starts
    ============================================ -->
    <nav class="navbar navbar-expand-lg">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="/images/whitelogo.png" style="width: 200px;" alt="brand logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                <span class="toggler-icon-bar bar1"></span>
                <span class="toggler-icon-bar bar2"></span>
                <span class="toggler-icon-bar bar3"></span>
            </button>
            <!-- End of .navbar-toggler -->

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                     <li class="nav-item">
                        <a class="nav-link" href="{{ url('/terms') }}">About & Terms
                        </a>
                    </li>
                     
                    <li class="nav-item">

                         @guest
                        <a class="nav-link" href="{{ url('/register') }}">Join Network
                        </a>
                        @else
                         <a class="nav-link" href="contact.html">Hi, {{ Auth::user()->name }}
                        </a>
                        @endguest
                    </li>
                    <li class="nav-item">
                         @guest
                        <a class="btn btn-small" href="{{ url('/login') }}">Login</a>
                         @else
                         <a class="btn btn-small" href="{{ url('/dashboard') }}">Dashboard</a>
                         @endguest
                    </li>
                </ul>
            </div>
        </div>
        <!-- End of .container -->
    </nav>
    <!-- End of .navbar -->

    <!-- banner starts
============================================ -->
    <section>
        <div class="banner-wrapper banner-wrapper__banner-inner d-flex align-items-center">
            <div class="container">
                <div class="banner-txt">
                    <h1>Terms &amp; Conditions</h1>
                </div>
                <!-- End of .banner-txt -->
            </div>
            <!-- End of .container -->
        </div>
        <!-- End of .banner-wrapper -->
    </section>
    <!-- End of .banner -->

    <!-- about starts
============================================ -->
    <section>
        <div class="container">
            <div class="privacy-content">
                <h4>Definitions</h4>
                    <div>
                    <p><strong>Assignment Agreement</strong> – an agreement concluded through the Portal and entered into upon the assignment of the Claim with which the Company assigns the Claim to the Buyer.</p>
                    <p><strong>Borrower</strong> – a person who has entered into the Loan Agreement as a borrower or person who has submitted an application for taking the Loan.</p>
                    <p><strong>Banking Day</strong> – a business day, i.e. any day, except Saturday, Sunday, a national or a public holiday of the Republic of Zimbabwe.</p>
                    <p><strong>Buyer</strong> – the User who has bought or has made an offer to buy the Claim from the Company or the Seller.</p>
                    <p><strong>Claim</strong> – a monetary claim deriving from the Loan Agreement against the Borrower. The exact composition of the Claim is provided in the Portal.</p>
                    <p><strong>Company</strong> – Streetwage Financial Services</p>
                   
                    <p><strong>Company Support e-mail</strong> – a contact e-mail address found at the Portal homepage.</p>
                   
                    <p><strong>Debt Servicing Cost</strong> – the costs related to debt collection and legal recovery process of the specific Borrower and compensated by the Buyer to the Company. These costs include all direct and actual costs related to debt collection (reminders, debt collection agency fees, the fee for collection, etc) and legal recovery (court fees, lawyer fees etc).</p>
                    <p><strong>Default</strong> - cancellation of the respective Loan Agreement in the event where Borrower has failed to make a scheduled payments in full under the Loan Agreement and more than one month has passed from the due date of the second repayment (which was not paid duly) and if an additional term of two weeks has been granted to the Borrower for the payment of unpaid amount borrowed (without due payment), if not agreed differently in the Loan Agreement or required by law in force. In case of Default, the payment of the whole debt (i.e. the Loan together with accrued interest, interest on arrears and fees) shall be claimed from the Borrower.</p>
                  
                    <p><strong>Loan</strong> – the principle amount lent by a credit provider to the Borrower in accordance with Loan Agreement and subject to repayment by the Borrower. </p>
                    <p><strong>Loan Agreement</strong> – a loan agreement concluded through the Portal between the Borrower and a credit provider.</p>
                    <p><strong>Portal</strong> – an e-service environment located at <a href="https://www.streetwage.com/dashboard" target="_blank">www.streetwage.com</a> on the Internet and administered by the Company, through which the Users can sell and purchase the Claims.</p>
                    <p><strong>Portal’s Mailbox</strong> – application of the Portal through which the Users can receive important personal messages when using the Portal and through which the Users are able to send personal messages to other Users, the Company or other persons operating in the Portal.</p>
                    <p><strong>Portfolio Manager</strong> – an application of the Portal that automatically submits transaction orders according to the terms and conditions determined by the Buyer.</p>
                    <p><strong>Portfolio Pro</strong> – an application of the Portal that automatically submits transaction orders according to the terms and conditions determined by the Buyer.</p>
                    <p><strong>Portal Account</strong> – User’s or the Company’s separate running account in the Portal regarding the settlements and transactions with the Company and other Users arising from the User Agreement, the Assignment Agreement, the Resale Agreement and other agreements.</p>
                    <p><strong>Purchase Price</strong> – a price of the Claim agreed between the Buyer and the Company in the Assignment Agreement or between the Buyer and the Seller in Resale Agreement.</p>
                    <p><strong>Reference Number</strong> – a personalised number for each User by which a User can be identified.</p>
                    <p><strong>Resale Agreement</strong> – an agreement concluded through the Portal in the secondary market between the Seller and the Buyer upon resale of a Claim.</p>
                    <p><strong>Seller</strong> – the User who sells the Claim belonging to him/her/itself via the secondary market managed by the Portal.</p>
                    <p><strong>Tender Management Process</strong> – a process of the Company under which the Company (a) determines the match between a Claim and the Tender Offer and (b) takes all necessary steps to prepare the assignment of the Claim in accordance with the Assignment Agreement.</p>
                    <p><strong>Tender Offer</strong> – a non-binding offer of the Buyer that determines the criteria which a Claim needs to meet in order for the Buyer to be willing to purchase a Claim from the Company or the Seller.</p>
                    <p><strong>Terms of Use</strong> – these Terms of Use, which are a part of the User Agreement.</p>
                    <p><strong>Trial Offer</strong> - the Company may from time to time offer to the User free cash for buying the Claims through Portal. The Company reserves the right to reclaim the Trial Offer in full if the respective User has received such Trial Offer and has not purchased Claims in amount of at least 10 euros within the 30 days of the receipt of such Trial Offer. In case the Trial Offer has been used for purchasing the Claim by the respective User the Company has a right to sell the Claim and transfer the funds received from such sale to the account of the Company or transfer the ownership of the respective Claim to the Company on behalf of the respective User. The Company is not obligated to notify the respective User of such reclaim of the Trial Offer or a sale of the Claim. </p>
                    <p><strong>User</strong> – a natural or a legal person who has entered into the User Agreement with the Company and has been registered as the user of the Portal.</p>
                    <p><strong>User Agreement</strong> – an agreement concluded between the User and the Company under which the Company provides services to the User via the Portal.</p>
                    </div>
                </li>
                

            <h2 id="privacy-policy" class="text-highlight">Streetwage Privacy Policy</h2>
            <p>This privacy policy gives you an overview of the data and information we collect from you when you use our services, how we use this collected information, who we share it with and what are your rights in relation to it.</p>
            <p>We follow very strict guidelines on handling your information, based on the European Union and the Estonian Personal Data Protection laws. This privacy policy is applicable to the Users who are natural persons and does not cover those Users who are legal persons.</p>
            <p>Terms used in this Privacy Policy have the respective meaning as described in the Terms of Use of Streetwage.</p>

            <h4>Information Collected</h4>
            <p>Information we collect from you can be divided to the following categories:</p>
            
                <p>Personal information (e.g name, personal identification code, date of birth, address, gender, identification document information, bank name, bank account number);
                Contact information (e.g email address, mobile phone);
                Transaction information and browsing data – (e.g services used on our website, IP address, browser type, preferences, investment goals, incoming payments, communication).</p>
          
           
            <p>We guarantee that access to User information is limited to the company's employees, whose duties require access to such data. All Streetwage employees are required to keep information confidential and are forbidden to share it with the third parties unless reasons stated above.</p>
            <h4>Information Stored</h4>
            <p>Information is stored and archived on a secure server with access limited to a number of people. Security is enforced through rigorous privacy protection standards performed by dedicated third party partners.</p>
            <h4>Information Security</h4>
            <p>A number of Internet security measures have been added to ensure that information is processed, transmitted and archived securely.</p>
           
            

            <h4>Your Rights</h4>
            <p>We need to inform you about what information is collected, from where information is collected, how information is used, to whom this information is shared, how long this information is kept and what your rights in relation to it are. This information is provided to you on our Website (if applicable) and in this Privacy Policy. </p>
            <p>You have a right to access your information. We must submit you the overview of the information we have on you unless there is a legal reasons not to comply with this request. We aim to provide you the information as soon as possible with free of charge. Please note when you request the personal information more than twice a year we have a right to charge a fee to cover our costs for providing you that information. Please e-mail us at streetwage@gmail.com </p>
            <p>You have a right to ask for correction of your inaccurate or incomplete date. Please e-mail us at <a href="/cdn-cgi/l/email-protection#761f1800130502190436141918121904175815191b"><span class="__cf_email__" data-cfemail="1d74736b786e69726f5d7f727379726f7c337e7270">[email&#160;protected]</span></a></p>
            <p>You have a right to ask us to erase your information. We have a right to refuse the request if the continuation of the information is necessary for the fulfilment of the contractual obligations or if such obligation arises from applicable legislation. Please e-mail us at <a href="/cdn-cgi/l/email-protection#3f5651495a4c4b504d7f5d50515b504d5e115c5052"><span class="__cf_email__" data-cfemail="650c0b130016110a1725070a0b010a17044b060a08">[email&#160;protected]</span></a> </p>
            <p>You have a right to ask us to restrict processing information and revoke your prior consent for such information processing. We have a right to refuse the request if processing is for the establishing or exercise of defense of legal claims. Please e-mail us at <a href="/cdn-cgi/l/email-protection#e1888f978492958e93a1838e8f858e9380cf828e8c"><span class="__cf_email__" data-cfemail="3c55524a594f48534e7c5e535258534e5d125f5351">[email&#160;protected]</span></a></p>
            <p>You have a right to receive your personal information submitted to us by yourself for purpose of transferring this information to another service provider. Please e-mail us at <a href="/cdn-cgi/l/email-protection#3851564e5d4b4c574a785a57565c574a59165b5755"><span class="__cf_email__" data-cfemail="7f1611091a0c0b100d3f1d10111b100d1e511c1012">[email&#160;protected]</span></a> </p>

            <h4>Retaining Information</h4>
            <p>We will keep your information until we are legally required to do so. When information processing and storing is no longer required under applicable legislation it is removed from the registry.</p>
            <h4>Cookies</h4>
            <p>We use “cookies” which are small text files placed on your hard drive by a web page server. This allows us to collect certain information from your web browser. Please see Cookie Policy for additional information.</p>
            <h4>Links to Third Party Websites</h4>
            <p>Links to third party websites from/to Bondora website are provided solely as an access point to obtain useful information on services or topics that may be needed to the users of our platform. Please note that third party websites may have different privacy policies and/or security standards governing their sites.</p>
            
            <p>This Policy was updated on 13th of March 2019.</p>

            <h2 id="cookie-policy" class="text-highlight">Cookie policy</h2>

            <h4>What is a Cookie?</h4>
            <p>A cookie is a small morsel of text that a website asks your browser to store. All cookies have expiration dates in them that determine how long they stay in your browser. Cookies can be removed in two ways: automatically, when they expire, or when you manually delete them. We've included more details below to help you understand what kinds of cookies we use.</p>
            <p>Please remember that by deleting existing cookies or disabling future cookies you may not be able to access certain areas or features of our website.</p>
            <h4>What Cookies Does the Company Use?</h4>
            <p>We use cookies across our website to improve the performance and enhance the user experience. Please be advised that when you visit our website you agree that we use the following cookies:</p>
            <p>Essential cookies that are only set or retrieved by the website while you are visiting which allow us to identify you as a subscriber and ensure that you can access the subscription only pages. We can ensure that no one is making changes to your profile, acquiring or selling the Claims on your behalf.</p>
            <p>Functionality cookies are used to allow us to remember your preferences on the website, such as language or username to customize your user experience.</p>
            <p>Performance cookies allow us to collect information how you use the website. All of the information collected is anonymous and is only used to help us improve the way our website works and measure the effectiveness of our advertising campaigns.</p>
            <p>Third party cookies are behaviourally targeted advertising cookies (e.g. Google Analytics) that allow us to record pages that are visited and the links that are followed. The information collected in non-identified.</p>
            <p>Secure cookies are used to ensure that any data in the cookie will be encrypted as it passes between the website and the browser. It helps us to protect the security of your account.</p>
            <h4>How to Change Cookies' Settings?</h4>
            <p>In case you want to change your preferences about the cookies that we use, please change your browser settings. For more information you may wish to visit <a href="http://www.aboutcookies.org/" target="_blank">http://www.aboutcookies.org/</a></p>

            <h2 class="text-highlight">Complaints Policy</h2>
            <p>We would like to avoid seeing you dissatisfied with our services, but if this happens, we ask to hear from you! Here is how you can do it:</p>
            <p>If you are dissatisfied with our answer, you can appeal the decision by emailing the previously received response to our customer support. Here is what will happen then:</p>
            

            <h2 class="text-highlight">Conflicts Policy</h2>
            <h4>Introduction</h4>
            <p>Any conflicts of interest, which may arise in relation to the Company, shall be identified and managed by the Company. We will maintain and manage all arrangements necessary to prevent a conflict from escalating to causing material damage to the interests of our Users. The given document states our conflicts of interest policy (here and further “Conflicts Policy”) in as much as it applies to our activities.</p>
            <p>We are fully committed to complying with our regulatory and legal obligations and maintaining the ethical standards. We require that all of our employees are contractually bound to comply with our Conflicts Policy and any breach of such may lead to disciplinary proceedings, not excluding discharge of services.</p>
            <h4>Potential or Actual Conflicts Indication</h4>
            <p>A conflict of interest may take place if any of the Company directors, employees, outsource partners or otherwise “relevant persons” linked to the Company by control is providing a service to the Users or engaging in activities on their own account, which may cause a material risk or damage to the Users; interests, for example where any of the mentioned above persons:</p>
           
            <h4>Potential or Actual Conflicts of Interest We Have Identified</h4>
            <p>We may as well make payments to the third parties for referring the Users to us; however an appropriate disclosure shall be made for such instances.</p>
            <p>We may also offer preferred access to the Portal and additional non-standard advisory services to institutional investors.</p>
            <h4>Employees</h4>
            <p>A conflict of interest may occur if the Company’s employee (or the employee’s family) is somehow associated to the other party of any given transaction, especially if such party is a User. This association may include being a director, significant shareholder or a consultant to any User. The Company’s employees are required to disclose any connection, which could compromise their judgement, or involve any material interests.</p>
            <h4>Managing Conflicts of Interest and Disclosure</h4>
            <p>The Company will make all possible and reasonable efforts to mediate any conflict of interest.</p>
            <p>Should there be a considerable risk of damage to any User, we will issue an appropriate disclosure.</p>
            <p>We will carry out all necessary procedures, including employee training, to ensure that circumstances at which conflict of interest may arise are identified and properly managed.</p>
            <p>All conflicts, which may arise, shall be closely monitored.</p>

            </div>
            <!-- End of .privacy-content -->
        </div>
        <!-- End of .container -->
    </section>
    <!-- End of section -->

    <!-- footer starts
============================================ -->
    <footer class="footer">
        <div class="radial-bg radial-bg__footer">
        </div>
        <!-- End of .radial-bg -->
        <div class="container">
            <div class="page-footer">
                <div class="footer-menu">
                    <div class="row">
                        <div class="col-md">
                            <a href="index.html" class="d-block">
                                <img src="/images/purplelogo.png" style="width: 150px;" alt="footer logo">
                            </a>
                        </div>
                        <!-- End of .col -->

                       


                        <div class="col-md">
                            <h5>Help</h5>
                            <ul class="footer-nav">
                                <li>
                                    <a class="active" href="{{ url('/terms') }}">Terms &amp; Conditions</a>
                                </li>
                                <li>
                                    <a href="{{ url('/terms') }}">Privacy Policy</a>
                                </li>
                               
                            </ul>
                            <!-- End of .footer-nav -->
                        </div>
                        <!-- End of .col -->

                        <div class="col-md-4">
                            <h5>Newsletter</h5>
                            <p>Sign up for our exclusive weekly newsletter.</p>
                            <form class="footer-form" action="#" method="post">
                                <input type="email" class="form-control" placeholder="Enter email address…">
                                <button class="icon-btn" type="submit">
                                    <i class="icon-arrow-right-1"></i>
                                </button>
                            </form>

                            <div class="social-icons">
                                <a href="https://www.facebook.com" target="_blank" rel="noopener">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                                <a href="https://www.twitter.com/Streetwage" target="_blank" rel="noopener">
                                    <i class="fab fa-twitter"></i>
                                </a>
                                <a href="https://www.instagram.com" target="_blank" rel="noopener">
                                    <i class="fab fa-instagram"></i>
                                </a>
                                <a href="https://www.youtube.com" target="_blank" rel="noopener">
                                    <i class="fab fa-youtube"></i>
                                </a>
                                <a href="https://www.linkedin.com/" target="_blank" rel="noopener">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                                <a href="https://www.pinterest.com" target="_blank" rel="noopener">
                                    <i class="fab fa-pinterest-p"></i>
                                </a>
                            </div>
                            <!-- End of .social-icons -->
                        </div>
                        <!-- End of .col -->
                    </div>
                    <!-- End of .row -->
                </div>
                <!-- End of .footer-menu -->

                <div class="footer-bottom">
                    <p>Copyright &copy; 2018. All rights reserved by
                        <a href="#">Your Company</a>
                    </p>
                    <div class="dropdown text-xs-right ml-auto">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Language:
                            <span>English</span>
                            <i class="fa fa-angle-up"></i>
                        </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">English US</a>
                            <a class="dropdown-item" href="#">English Uk</a>
                            <a class="dropdown-item" href="#">French</a>
                            <a class="dropdown-item" href="#">Bangla</a>
                        </div>
                        <!-- End of .dropdown-menu -->
                    </div>
                    <!-- End of .dropdown -->
                </div>
                <!-- End of .footer-copyright -->
            </div>
            <!-- End of .page-footer -->
        </div>
        <!-- End of .container -->
    </footer>
    <!-- End of page-footer -->


    <!-- Javascripts
    ======================================= -->
    <!-- jQuery -->
    <script src="/homepage/js/jquery-3.2.1.min.js"></script>
    <script src="/homepage/js/jquery-migrate.min.js"></script>
    <!-- Bootstrap js -->
    <script src="/homepage/js/bootstrap.bundle.min.js"></script>
    <!-- font awesome -->
    <script src="/homepage/js/fontawesome-all.min.js"></script>
    <!-- jQuery Countdown plugin -->
    <script src="/homepage/js/jquery.countdown.min.js"></script>
    <!-- jQuery Easing Plugin -->
    <script src="/homepage/js/easing-1.3.js"></script>
    <!-- jQuery progressbar plugin -->
    <script src="/homepage/js/jquery.waypoints.min.js"></script>
    <!-- Bootstrap Progressbar -->
    <script src="/homepage/js/bootstrap-progressbar.min.js"></script>
    <!-- ImagesLoaded js -->
    <script src="/homepage/js/imagesloaded.pkgd.min.js"></script>
    <!-- Slick carousel js -->
    <script src="/homepage/js/slick.min.js"></script>
    <!-- Magnific popup -->
    <script src="/homepage/js/jquery.magnific-popup.min.js"></script>
    <!-- Custom js -->
    <script src="/homepage/js/main.js"></script>


</body>

</html>