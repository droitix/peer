
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
    <title>BOOTCLASIFIED - Responsive Classified Theme</title>
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.css" rel="stylesheet">


    <link href="/css/style.css" rel="stylesheet">

    <!-- styles needed for carousel slider -->
    <link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet">

    <!-- bxSlider CSS file -->
    <link href="assets/plugins/bxslider/jquery.bxslider.css" rel="stylesheet"/>

    <!-- Just for debugging purposes. -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- include pace script for automatic web page progress bar  -->
    <script>
        paceOptions = {
            elements: true
        };
    </script>
    <script src="assets/js/pace.min.js"></script>
    <script src="assets/plugins/modernizr/modernizr-custom.js"></script>


</head>
<body>
<div id="wrapper">

  
    
    <div class="main-container">
        <div class="container">
            <div class="row">
                <!-- this (.mobile-filter-sidebar) part will be position fixed in mobile version -->
                <div class="col-md-3 page-sidebar mobile-filter-sidebar">
                    <aside>
                        <div class="sidebar-modern-inner">
                            <div class="block-title has-arrow sidebar-header">
                                <h5><a href="#">All Categories</a></h5>
                            </div>
                            <div class="block-content categories-list  list-filter ">
                                <ul class=" list-unstyled">
                                    <li><a href="sub-category-sub-location.html"><span class="title">Electronics</span><span class="count">&nbsp;8626</span></a>
                                    </li>
                                    <li><a href="sub-category-sub-location.html"><span class="title">Automobiles </span><span class="count">&nbsp;123</span></a></li>
                                    <li><a href="sub-category-sub-location.html"><span class="title">Property </span><span class="count">&nbsp;742</span></a></li>
                                    <li><a href="sub-category-sub-location.html"><span class="title">Services </span><span class="count">&nbsp;8525</span></a></li>
                                    <li><a href="sub-category-sub-location.html"><span class="title">For Sale </span><span class="count">&nbsp;357</span></a></li>
                                    <li><a href="sub-category-sub-location.html"><span class="title">Learning </span><span class="count">&nbsp;3576</span></a></li>
                                    <li><a href="sub-category-sub-location.html"><span class="title">Jobs </span><span class="count">&nbsp;453</span></a></li>
                                    <li><a href="sub-category-sub-location.html"><span class="title">Cars &amp; Vehicles</span><span class="count">&nbsp;801</span></a>
                                    </li>
                                    <li><a href="sub-category-sub-location.html"><span class="title">Other</span><span class="count">&nbsp;9803</span></a></li>
                                </ul>
                            </div>  <!--/.categories-list-->

                            <div class="block-title has-arrow ">
                                <h5><a href="#">Location</a></h5>
                            </div>
                            <div class="block-content locations-list  list-filter ">
                                <ul class="browse-list list-unstyled long-list">
                                    <li><a href="sub-category-sub-location.html"> Atlanta </a></li>
                                    <li><a href="sub-category-sub-location.html"> Wichita </a></li>
                                    <li><a href="sub-category-sub-location.html"> Anchorage </a></li>
                                    <li><a href="sub-category-sub-location.html"> Dallas </a></li>
                                    <li><a href="sub-category-sub-location.html">New York </a></li>
                                    <li><a href="sub-category-sub-location.html"> Santa Ana/Anaheim </a></li>
                                    <li><a href="sub-category-sub-location.html"> Miami </a></li>
                                    <li><a href="sub-category-sub-location.html"> Virginia Beach </a></li>
                                    <li><a href="sub-category-sub-location.html"> San Diego </a></li>
                                    <li><a href="sub-category-sub-location.html"> Boston </a></li>
                                    <li><a href="sub-category-sub-location.html"> Houston </a></li>
                                    <li><a href="sub-category-sub-location.html">Salt Lake City </a></li>
                                    <li><a href="sub-category-sub-location.html"> Other Locations </a></li>
                                </ul>
                            </div>
                            <!--/.locations-list-->

                            <div class="block-title has-arrow">
                                <h5><a href="#">Price range</a></h5>
                            </div>
                            <div class="block-content categories-list  list-filter ">

                                <form role="form" class="form-inline ">
                                    <div class="form-group col-lg-4 col-md-12 no-padding">
                                        <input type="text" placeholder="$ 2000 " id="minPrice" class="form-control">
                                    </div>
                                    <div class="form-group col-lg-1 col-md-12 no-padding text-center hidden-md"> -</div>
                                    <div class="form-group col-lg-4 col-md-12 no-padding">
                                        <input type="text" placeholder="$ 3000 " id="maxPrice" class="form-control">
                                    </div>
                                    <div class="form-group col-lg-3 col-md-12 no-padding">
                                        <button class="btn btn-default pull-right btn-block-md" type="submit">GO
                                        </button>
                                    </div>
                                </form>
                                <div style="clear:both"></div>
                            </div>
                            <!--/.list-filter-->
                            <div class="block-title has-arrow">
                                <h5><a href="#">Seller </a></h5>
                            </div>
                            <div class="block-content categories-list  list-filter ">
                                <ul class="browse-list list-unstyled ">
                                    <li><a href="sub-category-sub-location.html"><strong>All Ads</strong> <span class="count">228,705</span></a></li>
                                    <li><a href="sub-category-sub-location.html">Business <span class="count">28,705</span></a></li>
                                    <li><a href="sub-category-sub-location.html">Private <span class="count">18,705</span></a></li>
                                </ul>
                            </div>
                            <!--/.list-filter-->

                            <div class="block-title has-arrow">
                                <h5><a href="#">Condition</a></h5>
                            </div>
                            <div class="block-content categories-list  list-filter ">
                                <ul class="browse-list list-unstyled ">
                                    <li><a href="sub-category-sub-location.html">New <span class="count">228,705</span></a>
                                    </li>
                                    <li><a href="sub-category-sub-location.html">Used <span class="count">28,705</span></a>
                                    </li>
                                    <li><a href="sub-category-sub-location.html">None </a></li>
                                </ul>
                            </div>
                            <!--/.list-filter-->
                            <div style="clear:both"></div>
                        </div>



                        <!--/.categories-list-->
                    </aside>
                </div>
                <!--/.page-side-bar-->
                <div class="col-md-9 page-content col-thin-left">

                    <div class="category-list">
                        <div class="tab-box ">

                         
                            

                        </div>
                        <!--/.tab-box-->
                        <!--/.tab-box-->

                       
                        <!-- Mobile Filter bar-->
                        <div class="mobile-filter-bar col-xl-12  ">
                            <ul class="list-unstyled list-inline no-margin no-padding">
                                <li class="filter-toggle">
                                    <a class="">
                                        <i class="  icon-th-list"></i>
                                        Filters
                                    </a>
                                </li>
                                <li>


                                    <div class="dropdown">
                                        <a data-toggle="dropdown" class="dropdown-toggle">Short
                                            by </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="" rel="nofollow">Relevance</a></li>
                                            <li><a href="" rel="nofollow">Date</a></li>
                                            <li><a href="" rel="nofollow">Company</a></li>
                                        </ul>
                                    </div>

                                </li>
                            </ul>
                        </div>
                        <div class="menu-overly-mask"></div>
                        <!-- Mobile Filter bar End-->
                        <div class="tab-content">
                            <div class="tab-pane  active " id="alladslist">
                                <div class="adds-wrapper row no-margin property-list">
                                    <div class="item-list">

                                        <div class="row">


                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fas fa-camera"></i> 2 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="/images/55.jpg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive and modern luxury apartment  Union Avenue </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->




                                    <div class="item-list">

                                        <div class="row">
                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image">
                                                    <div id="properites-image-slide" class="carousel slide" data-ride="carousel"
                                                         data-interval="false">
                                                        <!-- Wrapper for slides -->
                                                        <div class="carousel-inner" role="listbox">
                                                            <div class="item active carousel-item">
                                                                <img src="/images/56.jpg" alt="...">
                                                            </div>
                                                            <div class="item carousel-item">
                                                                <img src="/images/57.jpg" alt="...">
                                                            </div>
                                                            <div class="item carousel-item">
                                                                <img src="/images/58.jpg" alt="...">
                                                            </div>
                                                        </div>
                                                        <!-- Controls -->

                                                        <a class="left carousel-control" href="#properites-image-slide" role="button"
                                                           data-slide="prev">

                                                            <span class="icon icon-left-open-big icon-prev" aria-hidden="true"></span>

                                                            <span class="sr-only">Previous</span>

                                                        </a>
                                                        <a class="right carousel-control"
                                                           href="#properites-image-slide" role="button" data-slide="next">

                                                            <span class="icon icon-right-open-big icon-next" aria-hidden="true"></span>

                                                            <span class="sr-only">Next</span>

                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Wow ! This item has a image slider ! </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">
                                        <div class="row">

                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 4 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="/images/59.jpg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive and modern luxury apartment  Union Avenue </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">

                                        <div class="row">
                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 3 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/13.jpeg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive and modern luxury apartment  Union Avenue </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">
                                        <div class="row">

                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 4 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/18.jpeg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Fully Furnished 2 Bedroom in Residence luxury apartment   </a></h5>
                                                    <span class="info-row"> <span class="item-location">Sports City , NY 25411 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">2+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">271 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">2</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$5400 - $7260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">

                                        <div class="row">
                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 4 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/b12.jpg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive Furnished and modern luxury apartment   </a></h5>
                                                    <span class="info-row"> <span class="item-location">Oceana Avenue, Brooklyn, NY 50154 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">321 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">
                                        <div class="row">

                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 6 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/14.jpeg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Majestic Atlantis View from a High Floor luxury apartment   </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">

                                        <div class="row">
                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 4 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/11.jpeg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive and modern  Atlantis View - Low Floor </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">
                                        <div class="row">

                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 4 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/16.jpg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive and modern luxury apartment  Union Avenue </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">

                                        <div class="row">
                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 8 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/building.jpg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive and modern luxury apartment  Union Avenue </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">
                                        <div class="row">

                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 4 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/18.jpeg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive and modern luxury apartment  Union Avenue </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                </div>
                                <!--/.adds-wrapper-->
                            </div>
                            <div class="tab-pane   " id="businessads">
                                <div class="adds-wrapper row no-margin property-list">

                                    <div class="item-list">
                                        <div class="row">

                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 4 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/18.jpeg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Fully Furnished 2 Bedroom in Residence luxury apartment   </a></h5>
                                                    <span class="info-row"> <span class="item-location">Sports City , NY 25411 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">2+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">271 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">2</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$5400 - $7260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">

                                        <div class="row">
                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 4 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/b12.jpg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive Furnished and modern luxury apartment   </a></h5>
                                                    <span class="info-row"> <span class="item-location">Oceana Avenue, Brooklyn, NY 50154 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">321 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">
                                        <div class="row">

                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 6 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/14.jpeg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Majestic Atlantis View from a High Floor luxury apartment   </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">

                                        <div class="row">
                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 4 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/11.jpeg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive and modern  Atlantis View - Low Floor </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->

                                    <div class="item-list">

                                        <div class="row">


                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 2 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/2.jpeg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive and modern luxury apartment  Union Avenue </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->




                                    <div class="item-list">

                                        <div class="row">
                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image">
                                                    <div id="properites-image-slide" class="carousel slide" data-ride="carousel"
                                                         data-interval="false">
                                                        <!-- Wrapper for slides -->
                                                        <div class="carousel-inner" role="listbox">
                                                            <div class="item active carousel-item">
                                                                <img src="images/house/thumb/4.jpeg" alt="...">
                                                            </div>
                                                            <div class="item carousel-item">
                                                                <img src="images/house/thumb/5.jpg" alt="...">
                                                            </div>
                                                            <div class="item carousel-item">
                                                                <img src="images/house/thumb/6.jpg" alt="...">
                                                            </div>
                                                        </div>
                                                        <!-- Controls -->

                                                        <a class="left carousel-control" href="#properites-image-slide" role="button"
                                                           data-slide="prev">

                                                            <span class="icon icon-left-open-big icon-prev" aria-hidden="true"></span>

                                                            <span class="sr-only">Previous</span>

                                                        </a>
                                                        <a class="right carousel-control"
                                                           href="#properites-image-slide" role="button" data-slide="next">

                                                            <span class="icon icon-right-open-big icon-next" aria-hidden="true"></span>

                                                            <span class="sr-only">Next</span>

                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Wow ! This item has a image slider ! </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">
                                        <div class="row">

                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 4 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/9.jpg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive and modern luxury apartment  Union Avenue </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">

                                        <div class="row">
                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 3 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/13.jpeg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive and modern luxury apartment  Union Avenue </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->



                                    <div class="item-list">
                                        <div class="row">

                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 4 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/16.jpg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive and modern luxury apartment  Union Avenue </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">

                                        <div class="row">
                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 8 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/building.jpg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive and modern luxury apartment  Union Avenue </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">
                                        <div class="row">

                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 4 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/18.jpeg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive and modern luxury apartment  Union Avenue </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                </div>
                                <!--/.adds-wrapper-->
                            </div>
                            <div class="tab-pane   " id="private">
                                <div class="adds-wrapper row no-margin property-list">


                                    <div class="item-list">

                                        <div class="row">
                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 3 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/13.jpeg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive and modern luxury apartment  Union Avenue </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->
                                    <div class="item-list">

                                        <div class="row">


                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 2 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/2.jpeg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive and modern luxury apartment  Union Avenue </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->




                                    <div class="item-list">

                                        <div class="row">
                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image">
                                                    <div id="properites-image-slide" class="carousel slide" data-ride="carousel"
                                                         data-interval="false">
                                                        <!-- Wrapper for slides -->
                                                        <div class="carousel-inner" role="listbox">
                                                            <div class="item active carousel-item">
                                                                <img src="images/house/thumb/4.jpeg" alt="...">
                                                            </div>
                                                            <div class="item carousel-item">
                                                                <img src="images/house/thumb/5.jpg" alt="...">
                                                            </div>
                                                            <div class="item carousel-item">
                                                                <img src="images/house/thumb/6.jpg" alt="...">
                                                            </div>
                                                        </div>
                                                        <!-- Controls -->

                                                        <a class="left carousel-control" href="#properites-image-slide" role="button"
                                                           data-slide="prev">

                                                            <span class="icon icon-left-open-big icon-prev" aria-hidden="true"></span>

                                                            <span class="sr-only">Previous</span>

                                                        </a>
                                                        <a class="right carousel-control"
                                                           href="#properites-image-slide" role="button" data-slide="next">

                                                            <span class="icon icon-right-open-big icon-next" aria-hidden="true"></span>

                                                            <span class="sr-only">Next</span>

                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Wow ! This item has a image slider ! </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">
                                        <div class="row">

                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 4 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/9.jpg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive and modern luxury apartment  Union Avenue </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">
                                        <div class="row">

                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 4 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/18.jpeg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Fully Furnished 2 Bedroom in Residence luxury apartment   </a></h5>
                                                    <span class="info-row"> <span class="item-location">Sports City , NY 25411 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">2+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">271 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">2</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$5400 - $7260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">

                                        <div class="row">
                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 4 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/b12.jpg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive Furnished and modern luxury apartment   </a></h5>
                                                    <span class="info-row"> <span class="item-location">Oceana Avenue, Brooklyn, NY 50154 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">321 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">
                                        <div class="row">

                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 6 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/14.jpeg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Majestic Atlantis View from a High Floor luxury apartment   </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">

                                        <div class="row">
                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 4 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/11.jpeg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive and modern  Atlantis View - Low Floor </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">
                                        <div class="row">

                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 4 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/16.jpg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive and modern luxury apartment  Union Avenue </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">

                                        <div class="row">
                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 8 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/building.jpg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive and modern luxury apartment  Union Avenue </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                    </div>
                                    <!--/.item-list-->


                                    <div class="item-list">
                                        <div class="row">

                                            <div class="col-md-3 no-padding photobox">
                                                <div class="add-image"><span class="photo-count"><i
                                                        class="fa fa-camera"></i> 4 </span> <a href="property-details.html"><img
                                                        class="thumbnail no-margin" src="images/house/thumb/18.jpeg"
                                                        alt="img"></a></div>
                                            </div>
                                            <!--/.photobox-->
                                            <div class="col-md-6 add-desc-box">
                                                <div class="ads-details">
                                                    <h5 class="add-title"><a href="property-details.html">
                                                        Exclusive and modern luxury apartment  Union Avenue </a></h5>
                                                    <span class="info-row"> <span class="item-location">544 Union Avenue, Brooklyn, NY 11211 |  <a><i
                                                            class="fa fa-map-marker-alt"></i> Map</a>  </span> </span>
                                                    <div class="prop-info-box">

                                                        <div class="prop-info">
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title ">4+2</span>
                                                                <span class="text">Adults | Children</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block middle">
                                                                <span class="title prop-area">171 m²</span>
                                                                <span class="text">Area (ca.)</span>
                                                            </div>
                                                            <div class="clearfix prop-info-block">
                                                                <span class="title prop-room">4</span>
                                                                <span class="text">room </span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/.add-desc-box-->
                                            <div class="col-md-3 text-right  price-box">


                                                <a class="btn btn-border-thin  btn-save"   title="save ads"  data-toggle="tooltip" data-placement="left">
                                                    <i class="icon icon-heart"></i>
                                                </a>
                                                <a class="btn btn-border-thin  btn-share ">
                                                    <i class="icon icon-export" data-toggle="tooltip" data-placement="left"  title="share"></i>
                                                </a>

                                                <h3 class="item-price "> <strong>$2400 - $4260 </strong></h3>
                                                <div style="clear: both"></div>

                                                <a class="btn btn-success btn-sm bold" href="property-details.html">
                                                    Check Availabilty
                                                </a>


                                            </div>
                                            <!--/.add-desc-box-->
                                        </div>
                                     </div>
                                    <!--/.item-list-->


                                </div>
                                <!--/.adds-wrapper-->
                            </div>
                          </div>



                        

                </div>
                <!--/.page-content-->

            </div>
        </div>
    </div>
    <!-- /.main-container -->


</div>
<!-- /.wrapper -->



<!-- Le javascript
================================================== -->

<!-- Placed at the end of the document so the pages load faster -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery/jquery-3.3.1.min.js">\x3C/script>')</script>

<script src="assets/js/vendors.min.js"></script>

<!-- include custom script for site  -->
<script src="assets/js/main.min.js"></script>




</body>
</html>
