@extends('layouts.app')

@section('content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h6 class="page-title-heading mr-0 mr-r-5">Account Funding</h6>
                    <p class="page-title-description mr-0 d-none d-md-inline-block"></p>
                </div>
                <!-- /.page-title-left -->
                <div class="page-title-right d-none d-sm-inline-flex">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active">Deposit</li>
                    </ol>
                </div>
                <!-- /.page-title-right -->
            </div>
            <!-- /.page-title -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="widget-list">
                <div class="row">
                    <div class="col-md-6 widget-holder">
                    <div class="widget-bg">
                        <div class="widget-body clearfix">
                            <h5 class="box-title">Ecocash Deposit Portal</h5>
                            <p>Click the ecocash button and begin</p><a href="#" class="btn btn-primary ripple mr-3" data-toggle="modal" data-target="#signup-modal">Ecocash</a>  
                            <!-- Signup Modal -->
                            <div id="signup-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <div class="modal-body">
                                            <div class="text-center my-3"><a href="#"><span><img src="/assets/demo/logo-expand-dark.png" alt=""></span></a>
                                            </div>
                                                  <h5 class="box-title"><i class="feather feather-info "></i> Ecocash Deposit Instructions</h5>
                                                  <p>1.Dial *151*3*1*10578*AMOUNT# TO PEER INVESTMENTS (min $20)</p>
                                                  <p>2.After sending fill in the form below.</p>
                                                  <p>3.Submit the form and in a few minutes we credit your account.</p>

                                            <form>
                                                <div class="form-group">
                                                    <label for="username">Full Ecocash sender Name</label>
                                                    <input class="form-control" type="text" id="username" required="" placeholder="Given Mutize">
                                                </div>
                                                <div class="form-group">
                                                    <label for="code">MP Code from message recieved</label>
                                                    <input class="form-control" type="code" id="code" required="" placeholder="mp.1204.34.l98">
                                                </div>
                                                <div class="form-group mr-b-30">
                                                    <label for="amount">Amount</label>
                                                    <input class="form-control" type="amount" required="" id="amount" placeholder="Enter your amount">
                                                </div>
                                                <div class="form-group mr-b-30">
                                                    <div class="checkbox checkbox-primary">
                                                        <label for="checkbox11">
                                                            <input id="checkbox11" type="checkbox"> <span class="label-text">I accept <a href="#">Terms and Conditions</a></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="text-center mr-b-30">
                                                    <button class="btn btn-rounded btn-success ripple" type="submit">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                         
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                </div>
                <!-- /.widget-holder -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.widget-list -->
        </main>
        <!-- /.main-wrappper -->
@endsection
