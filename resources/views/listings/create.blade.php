@extends('layouts.app')



@section('content')

    <main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h6 class="page-title-heading mr-0 mr-r-5">Buy Coins And Profit</h6>
                    <p class="page-title-description mr-0 d-none d-md-inline-block">Refer your friend and get 5% of every deposit they make instantly!</p>
                </div>
                <!-- /.page-title-left -->
                <div class="page-title-right d-none d-sm-inline-flex">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active">Form Validation</li>
                    </ol>
                </div>
                <!-- /.page-title-right -->
            </div>
            <!-- /.page-title -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="widget-list">
                <div class="row">



                    <div class="col-md-6 widget-holder">
                        <div class="widget-bg">
                            <div class="widget-body clearfix">

                                                  <h5 class="box-title"><i class="feather feather-info "></i> Bank Deposit Instructions</h5>
                                                  <p style="font-weight: bold;">1.Fill in the form with the amount you want to deposit and make sure your Account number is reflecting on your profile</p>
                                                  <p>2.Fill the form.</p>
                                                  <p>3.Once your deposit request is initiated, support will contact you with details of finalizing the deposit.</p>
                                                  <p style="font-weight: bold;"></p>




                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->

                    <div class="col-md-6 widget-holder">
                        <div class="widget-bg">
                            <div class="widget-body clearfix">
                                <h5 class="box-title mr-b-0">Deposit instructions</h5>
                                <p class="text-muted">Fill in this form</p>
                                <p class="text-muted">Step 1</p>
                                <hr>
                                <h4 class="box-title mr-b-0">Deposit in FNB Account 674666477783 Branch Code 5677</h4>
                                <p class="text-muted">or</p>
                                <h4 class="box-title mr-b-0">Deposit in FNB Account 674666477783 Branch Code 5677</h4>
                                <hr>
                                <p class="text-muted">Step 2</p>
                                 <form action="{{ route('listings.store', [$area]) }}" method="post">
                            <input type="hidden" name="area_id"  value="2" id="area_id">
                            <input type="hidden" name="category_id" value="2" id="category_id">
                             <input type="hidden" name="state" value="0" id="state">
                                    <!-- /.form-group -->
                                     <div class="form-group row">
                                       <div class="col-md-8">
                                        <label  for="fullname"> Name</label>
                                        <div class="input-group">

                                            <input class="form-control" name="fullname" id="fullname" placeholder="Full Name" type="text" required="">

                                        </div>

                                    </div>

                                    </div>
                                    <div class="form-group row">
                                       <div class="col-md-8">
                                        <label  for="code">Contact Number</label>
                                        <div class="input-group">

                                            <input class="form-control" name="code" id="code" placeholder="Your number which we can contact you" type="text" required>

                                        </div>

                                    </div>

                                    </div>
                                   <div class="form-group row">
                                       <div class="col-md-8">
                                        <label class="sr-only" for="exampleInputAmount">Amount</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <input class="form-control" name="amount" id="amount" placeholder="Amount" type="text" required>
                                            <div class="input-group-addon">.00</div>
                                        </div>

                                    </div>

                                    </div>
                                    <div class="form-group row">
                                        <button class="btn btn-dark" type="submit">Deposit</button>

                                    </div>
                                  {{ csrf_field() }}
                                </form>
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.widget-list -->
            <footer class="footer"><span class="heading-font-family">Copyright @ 2019. All rights reserved Streetwage</span>
    </footer>
        </main>
        <!-- /.main-wrappper -->


@endsection
