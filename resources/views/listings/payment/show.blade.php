@extends('layouts.v2adminapp')

@section('content')
   <div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="box_general padding_bottom"  style="margin-top:10px; margin-left: 15vw;">
                <div class="panel panel-default">
                   
                    <div class="panel-body text-center">
                        @if ($listing->cost() == 0)
                            <form action="{{ route('listings.payment.update', [$area, $listing]) }}" method="post">
                                <div role="tabpanel" class="tab-pane section close-account" id="close-account">
                            <h1>Your listing will now go live</h1>
                            <span>A few minutes of moderation should be factored</span>
                            <div class="buttons">
                                  <button type="submit" class="btn btn-primary">Complete</button>
                                
                            </div>
                        </div><!-- /.tab-pane -->
                                

                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}
                            </form>
                        @else
                            <p>Total cost: £{{ number_format($listing->cost(), 2) }}</p>
                            <payment-form></payment-form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
