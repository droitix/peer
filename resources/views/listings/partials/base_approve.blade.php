                                       <tr data-expanded="true">
                                            <td>{{ $listing->id }}2019</td>
                                            <td>
                                              @if (!$listing->state()) 
                                                {{ $listing->fullname }}
                                              @else
                                              {{ $listing->user->name }} {{ $listing->user->surname }}
                                              @endif
                                            </td>
                                            <td>{{ $listing->user->email }}</td>
                                            <td>{{ $listing->code }}</td>
                                             <td>@if ($listing->state()) 
                                                  Withdrawal
                                                 @else
                                                  Deposit
                                                 @endif
                                             </td>
                                           
                                            <td>{{ $listing->created_at }}</td>
                                           
                                             <td>$ {{ $listing->amount }}</td>
                                        </tr>