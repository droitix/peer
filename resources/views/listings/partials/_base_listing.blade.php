                        <tr>
                            <td><span class="headings-color"># {{ $listing->id }}2019</span>
                                </td>
                                    <td class="w-25"><span class="headings-color fw-bold">Ecocash RTGS$</span>
                                    </td>
                                    <td><span class="text-muted">$ {{ $listing->amount }}</span>
                                    </td>
                                    <td>
                                        @if ($listing->state()) 
                                            <div class="d-flex"><a href="#" class="btn btn-xs px-3 mr-3 fw-900 fs-9 text-uppercase btn-outline-dark flex-1 justify-content-center">Withdrawal</a> 
                                        @else 
                                        <div class="d-flex"><a href="#" class="btn btn-xs px-3 mr-3 fw-900 fs-9 text-uppercase btn-outline-success flex-1 justify-content-center">Deposit</a> 

                                     <a href="#" class="btn btn-xs px-0 content-color flex-2">{{ $listing->created_at->diffForHumans() }}</i></a>
                                            </div>
                                            <!-- /.d-flex -->
                                    @endif
                            </td>
                        </tr>