                        <tr>
                            <td><span class="headings-color"># {{ $listing->id }}2019</span>
                                </td>
                                @if ($listing->state()) 
                                    <td class="w-25"><span class="headings-color fw-bold">Withdrawal</span>
                                @else 
                                <td class="w-25"><span class="headings-color fw-bold">Deposit</span>
                                @endif 
                                        @if (session()->has('impersonate')) <a href="{{ route('listings.edit', [$area, $listing]) }}">Edit</a>
                                        @endif
                                    </td>
                                    <td><span class="text-muted">$ {{ $listing->amount }}</span>
                                    </td>
                                    <td>
                                        @if ($listing->declined())
                                         <div class="d-flex">
                                        <a 
                                        href="#" data-toggle="tooltip" data-placement="top" title="{{ $listing->message }} Contact support!" class="btn btn-xs px-3 mr-3 fw-900 fs-9 text-uppercase btn-outline-danger flex-1 justify-content-center"><i class="feather feather-alert-circle"></i>  Declined</a>  <a href="#" class="btn btn-xs px-0 content-color flex-2">{{ $listing->created_at }}</i></a>
                                            </div>
                                            <!-- /.d-flex -->
                                        

                                        @elseif (!$listing->live()) 
                                      <div class="d-flex">
                                        <a 
                                        href="#" href="#" data-toggle="tooltip" data-placement="top" title="{{ $listing->message }} Verifying Funds!" class="btn btn-xs px-3 mr-3 fw-900 fs-9 text-uppercase btn-outline-warning flex-1 justify-content-center">Pending Approval</a>  <a href="#" class="btn btn-xs px-0 content-color flex-2">{{ $listing->created_at }}</i></a>
                                            </div>
                                            <!-- /.d-flex -->
                                        
            
        
                                        @else
                                         <div class="d-flex"><a href="#" data-toggle="tooltip" data-placement="top" title="{{ $listing->message }} Thank you!" class="btn btn-xs px-3 mr-3 fw-900 fs-9 text-uppercase btn-outline-success flex-1 justify-content-center">Approved</a>  <a href="#" class="btn btn-xs px-0 content-color flex-2">{{ $listing->created_at }}</i></a>
                                            </div>
                                            <!-- /.d-flex -->
                                        @endif
                            </td>
                        </tr>