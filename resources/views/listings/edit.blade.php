@extends('layouts.app')



@section('content')
    
    <main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h6 class="page-title-heading mr-0 mr-r-5">Approve</h6>
                    <p class="page-title-description mr-0 d-none d-md-inline-block">
                      @if ($listing->state()) 
                      Withdrawal 
                     @else
                     Deposit
                     @endif
                      Request of ${{ $listing->amount }} By {{ $listing->user->name }} {{ $listing->user->surname }}</p>
                </div>
                <!-- /.page-title-left -->
                <div class="page-title-right d-none d-sm-inline-flex">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active">Form Validation</li>
                    </ol>
                </div>
                <!-- /.page-title-right -->
            </div>
            <!-- /.page-title -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="widget-list">
                <div class="row">
                   
                    

                  

                    <div class="col-md-6 widget-holder">
                        <div class="widget-bg">
                            <div class="widget-body clearfix">
                                <h5 class="box-title mr-b-0">@if ($listing->state()) 
                      Withdrawal Request
                     @else
                     Deposit Request
                     @endif</h5>
                                <p class="text-muted">Verify Figures Before Approving</p>
                                 <form action="{{ route('listings.update', [$area, $listing]) }}" method="post">
                            <input type="hidden" name="area_id"  value="2" id="area_id">
                            <input type="hidden" name="category_id" value="2" id="category_id">
                            <input type="hidden" name="fullname" value="{{ $listing->fullname }}" id="fullname">
                            <input type="hidden" name="code" value="{{ $listing->code }}" id="code">
                            <input type="hidden" name="amount" value="{{ $listing->amount }}" id="amount">
                           <div class="form-group">
                          <label for="declined">Approve Transaction</label>
                            <select class="form-control" name="live" id="live">
                             <option>0</option>
                             <option>1</option>
      
                              </select>
                        </div> 
                        <div class="form-group">
                          <label for="declined">Void Transaction</label>
                            <select class="form-control" name="declined" id="declined">
                             <option>0</option>
                             <option>1</option>
      
                              </select>
                        </div>
                        <div class="form-group">
                          <label for="message">Message</label>
                            <select class="form-control" name="message" id="message">
                             <option>Funds verified</option>
                             <option>Failed to verify funds</option>
                             <option>Duplicate</option>
                             <option>Insuffcient funds</option>
                             <option>Balance not yet mature</option>
                             <option>Funds Sent to {{ $listing->user->ecocash }}</option>
      
                              </select>
                        </div>
                                 
                                                                   
                                    <div class="form-group row">
                                        <button class="btn btn-success" type="submit">Approve</button>
                                       
                                    </div>
                                  {{ csrf_field() }}
                                  {{ method_field('PATCH') }}
                                </form>
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.widget-list -->
            <footer class="footer"><span class="heading-font-family">Copyright @ 2019. All rights reserved Streetwage</span>
    </footer>
        </main>
        <!-- /.main-wrappper -->
    

@endsection
