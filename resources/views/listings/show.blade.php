@extends('layouts.app')

@section('content')
  <main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h6 class="page-title-heading mr-0 mr-r-5">Investment statement </h6>
                    <p class="page-title-description mr-0 d-none d-md-inline-block"></p>
                </div>
                <!-- /.page-title-left -->
                <div class="page-title-right d-none d-sm-inline-flex">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active">Statement</li>
                    </ol>
                </div>
                <!-- /.page-title-right -->
            </div>
            <!-- /.page-title -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="widget-list">
                <div class="row">
                    <div class="col-md-12 widget-holder">
                        <div class="widget-bg">
                            <div class="widget-body clearfix">
                                <div class="ecommerce-invoice">
                                    <div class="d-flex">
                                        
                                    </div>
                                    <!-- /.row -->
                                    <hr>
                                    <div class="d-flex">
                                        <div class="col-md-6 text-muted">
                                            <h6 class="mr-t-0">Invoice To</h6>{{ $listing->user->name }} {{ $listing->user->surname }}
                                            <br>{{ $listing->user->location }}
                                            <br>{{ $listing->user->phone }}
                                            <br>Zimbabwe</div>
                                        <div class="col-md-6 text-right">
                                            <h6 class="mr-t-0">Details:</h6><strong>Date:</strong>  <span class="text-muted">{{ $listing->created_at }}</span>
                                            <br><strong>ID:</strong>  <span class="text-muted">{{ $listing->id }}2019{{ $listing->id }}</span>
                                            <br><strong>Amount Invested:</strong>  <span class="text-muted">&dollar; {{ $listing->amount }}</span>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                    <hr class="border-0">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr class="bg-primary-dark text-white">
                                                <th class="text-center">&#35;</th>
                                                <th>Timeframe</th>
                                                <th class="text-center">Amount</th>
                                                <th class="text-center">Interest Rate</th>
                                                <th class="text-center">Expected Profit</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="text-center">{{ $listing->id }}2019{{ $listing->id }}</td>
                                                <td>{{ $listing->category->name }}</td>
                                                <td class="text-center">&dollar; {{ $listing->amount }}</td>
                                                <td class="text-center">0.30</td>
                                                <td class="text-center">&dollar; <?php echo 0.30*$listing->amount ?></td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <p>Thanks for being part of the peer community</p>
                                            <ul class="text-muted small">
                                                <li>Aeserunt tenetur cum nihil repudiandae perferendis fuga vitae corporis!</li>
                                                <li>Laborum, necessitatibus recusandae ullam at iusto dolore.</li>
                                                <li>Voluptatum aperiam voluptates quasi!</li>
                                                <li>Assumenda, iusto, consequuntur corporis atque culpa saepe magnam recusandae</li>
                                                <li>Possimus odio ipsam magni sint reiciendis unde amet</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4 invoice-sum text-right">
                                            <ul class="list-unstyled">
                                                <li>Total amount invested: &#36; {{ $listing->amount }}</li>
                                                <li>Profit (30%):&dollar; <?php echo 0.30*$listing->amount ?></li>
                                                <li>Maturity days : &#36;14</li>
                                                <li><strong>Total available: &#36; <?php  echo 1.30*$listing->amount ?></strong>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.ecommerce-invoice -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.widget-list -->
            <footer class="footer"><span class="heading-font-family">Copyright @ 2019. All rights reserved Streetwage</span>
    </footer>
        </main>
        <!-- /.main-wrappper -->
   
@endsection
