@extends('layouts.app')



@section('content')
    
    <main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h6 class="page-title-heading mr-0 mr-r-5">Withdrawal</h6>
                    <p class="page-title-description mr-0 d-none d-md-inline-block">request a withdrawal</p>
                </div>
                <!-- /.page-title-left -->
                <div class="page-title-right d-none d-sm-inline-flex">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active">withdrawal</li>
                    </ol>
                </div>
                <!-- /.page-title-right -->
            </div>
            <!-- /.page-title -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="widget-list">
                <div class="row">
                   
                    

                   

                    <div class="col-md-6 widget-holder">
                        <div class="widget-bg">
                            <div class="widget-body clearfix">
                                <h5 class="box-title mr-b-0">Ecocash Withdrawal</h5>
                                <p class="text-muted">Enter amount </p>
                                 <form action="{{ route('listings.store', [$area]) }}" method="post">
                            <input type="hidden" name="area_id"  value="2" id="area_id">
                            <input type="hidden" name="category_id" value="2" id="category_id">
                             <input type="hidden" name="state" value="1" id="state">
                                    <!-- /.form-group -->
                                    

                                     
                                   
                                   <div class="form-group row">
                                       <div class="col-md-8">
                                        <label class="sr-only" for="exampleInputAmount">Amount</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <input class="form-control" name="amount" id="amount" placeholder="Amount" type="text" required>
                                            <div class="input-group-addon">.00</div>
                                        </div>
                                    
                                    </div>

                                    </div>                                    
                                    <div class="form-group row">
                                        <button class="btn btn-dark" type="submit">Withdraw</button>
                                       
                                    </div>
                                  {{ csrf_field() }}
                                </form>
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                     <div class="col-md-6 widget-holder">
                        <div class="widget-bg">
                            <div class="widget-body clearfix">
                                
                                                  <h5 class="box-title"><i class="feather feather-info "></i> Withdrawal Instructions</h5>
                                                  <p>1.Enter the amount you want to withdraw</p>
                                                  <p>2.Click withdraw and a request will be placed.</p>
                                                  <p>3.The system will process your withdrawal.</p>
                                



                          
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.widget-list -->
            <footer class="footer"><span class="heading-font-family">Copyright @ 2019. All rights reserved Streetwage</span>
    </footer>
        </main>
        <!-- /.main-wrappper -->
    

@endsection
