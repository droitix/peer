@extends('layouts.v2applist')

@section('content')
<main>
@include('includes.v2bannerlist')


<div class="container margin_60_35">
            
            <div class="row">
                @if ($listings->count())
        @foreach ($listings as $listing)
                  @include ('listings.partials._listing', compact('listing'))
                @endforeach

            </div>
            <!-- /row -->
            {{ $listings->links() }}
            

             @else
                 <span>No listings</span>
                 @endif
            
        </div>
        <!-- /container -->
</main>

</body>
</html>

@endsection
