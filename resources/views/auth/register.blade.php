@extends('layouts.loginapp')

@section('content')
<body class="body-bg-full profile-page" style="background-image: url(/asset/img/site-bg.jpg)">
    <div id="wrapper" class="row wrapper">
        <div class="col-10 ml-sm-auto col-sm-8 col-md-4 ml-md-auto login-center mx-auto">
            <div class="navbar-header text-center mt-2 mb-4">
                <a href="{{ url('/dashboard') }}">
                    <img alt="" style="width:200px;" src="/images/purplelogo.png">
                </a>
            </div>
            <!-- /.navbar-header -->
            <h5 style="text-align:center; ">Create Platinum Auction Account</h5>
            <p>Signing up is easy. It only takes a few steps and you'll be up and running in no time.</p>
           <form method="POST" action="{{ route('register') }}">
                        @csrf
                <div class="form-group">
                    <label>Name</label>
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Name" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                </div>
                <div class="form-group">
                    <label>Last Name</label>
                    <input id="surname" type="text" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" value="{{ old('surname') }}" placeholder="Last Name" required autofocus>

                                @if ($errors->has('surname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                </div>

                <div class="form-group">
                    <label>Confirm Password</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                </div>
                 <div class="form-group">
                    <label>Refferal Promo Email(Enter if reffered you get extra Balance)</label>
                    <input id="promo" type="promo" class="form-control{{ $errors->has('promo') ? ' is-invalid' : '' }}" name="promo" value="{{ old('promo') }}" placeholder="Email of person who refered you(optional)">

                                @if ($errors->has('promo'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('promo') }}</strong>
                                    </span>
                                @endif
                </div>
                <div class="form-group no-gutters mb-3">
                    <div class="checkbox checkbox-primary">
                        <label>
                            <input type="checkbox"> <span class="label-text">I agree to all <a href="#">Terms &amp; Conditions</a></span>
                        </label>
                    </div>
                    <!-- /.checkbox -->
                </div>
                <!-- /.form-group -->
                <div class="form-group text-center no-gutters mb-4">
                    <button class="btn btn-block btn-lg btn-dark text-uppercase fs-12 fw-600" type="submit">Create Account</button>
                </div>
                <hr class="mb-4">

                <!-- /.btn-list -->
            </form>
            <!-- /.form-horzontal -->
            <footer class="col-sm-12 text-center">
                <hr>
                <p>Already have an account? <a href="{{ url('login') }}" class="text-primary m-l-5"><b>Log In</b></a>
                </p>
            </footer>
        </div>
        <!-- /.login-center -->
    </div>
    <!-- /.body-container -->
    <!-- Scripts -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="assets/js/material-design.js"></script>
</body>

@endsection
