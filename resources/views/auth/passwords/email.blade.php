@extends('layouts.loginapp')

@section('content')
<body class="body-bg-full profile-page" style="background-image: url(/asset/img/site-bg.jpg)">
    <div id="wrapper" class="wrapper">
        <div class="row container-min-full-height align-items-center">
            <div class="col-10 ml-sm-auto col-sm-6 col-md-4 ml-md-auto login-center login-center-mini mx-auto">
                <div class="navbar-header text-center mt-2 mb-4">
                    <a href="{{ url('/') }}">
                        <img alt="" style="width: 200px;" src="/images/purplelogo.png">
                    </a>
                </div>
                <!-- /.navbar-header -->
            
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                <form method="POST" action="{{ route('password.email') }}" class="form-material">
                    @csrf

                    
 
                    <p class="text-center text-muted">Enter your email address and we'll send you an email with instructions to reset your password.</p>
                    <div class="form-group no-gutters">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        <label for="example-email" class="col-md-12 mb-1">Email</label>
                    </div>
                    <div class="form-group mb-5">
                        <button class="btn btn-block btn-lg btn-dark text-uppercase fs-12 fw-600" type="submit">Submit</button>
                    </div>
                </form>
                <!-- /.form-material -->
                <footer class="col-sm-12 text-center">
                    <hr>
                    <p>Back to <a href="{{ url('login') }}" class="text-primary m-l-5"><b>Login</b></a>
                    </p>
                </footer>
            </div>
            <!-- /.login-right -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.body-container -->
    <!-- Scripts -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="/assets/js/material-design.js"></script>
</body>
@endsection
