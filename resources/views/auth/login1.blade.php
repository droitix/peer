@extends('layouts.app')

@section('content')
  <div class="tr-breadcrumb bg-image section-before">
        <div class="container">
            <div class="breadcrumb-info text-center">
                <div class="page-title">
                    <h1>Sign In</h1>
                    <span>Signing and start listing</span>
                </div>              
            </div>
        </div><!-- /.container -->
    </div><!-- tr-breadcrumb -->    

    <div class="tr-account section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="user-account">
                        <div class="account-content">
                            <form method="POST" class="tr-form" action="{{ route('login') }}">
                               @csrf

                        
                          

                         
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                       
                                 <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                      
                  

                     
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                  

                     
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                         
                    </form>

                    <div class="form-group">
                 @if (Request::has('previous'))
                    <input type="hidden" name="previous" value="{{ Request::get('previous') }}">
                @else
               <input type="hidden" name="previous" value="{{ URL::previous() }}">
                  @endif
                  </div>
                            <div class="new-user text-center">
                                <span><a href="{{ route('register') }}">Create a New Account</a> </span>
                            </div>
                        </div>
                    </div>          
                </div>
            </div><!-- /.row -->            
        </div><!-- container -->
    </div><!-- /.tr-account-->  

@endsection
