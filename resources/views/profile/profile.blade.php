

@extends('layouts.app')

@section('content')

  <main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h6 class="page-title-heading mr-0 mr-r-5">Your Profile</h6>
                    <p class="page-title-description mr-0 d-none d-md-inline-block">statistics, charts and events</p>
                </div>
                <!-- /.page-title-left -->
                <div class="page-title-right d-none d-sm-inline-flex">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active">Your Profile</li>
                    </ol>
                </div>
                <!-- /.page-title-right -->
            </div>
            <!-- /.page-title -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="widget-list">
                <div class="row">
                    <!-- User Summary -->
                    <div class="col-12 col-md-4 widget-holder widget-full-content">
                        <div class="widget-bg">
                            <div class="widget-body clearfix">
                                <div class="widget-user-profile">
                                    
                                    <div class="profile-body">
                                        <figure class="profile-user-avatar thumb-md">
                                            <img src="/uploads/avatars/{{ Auth::user()->avatar }}" alt="User Wall">
                                        </figure>
                                        <h6 class="h3 profile-user-name">{{ Auth::user()->name }}</h6><small class="profile-user-address">Harare, Zimbabwe</small>
                                        <hr class="profile-seperator">
                                       
                                        
                                    </div>
                                    <!-- /.d-flex -->
                                    <div class="row columns-border-bw border-top">
                                        <div class="col-4 d-flex flex-column justify-content-center align-items-center py-4">
                                            <h6 class="my-0"><span class="counter">$ {{ Auth::user()->balance }}.00</span></h6><small>Balance</small>
                                        </div>
                                        <!-- /.col-4 -->
                                        <div class="col-4 d-flex flex-column justify-content-center align-items-center py-4">
                                            <h6 class="my-0"><span class="counter">$0.00</span></h6><small>Refferal Profit</small>
                                        </div>
                                        <!-- /.col-4 -->
                                        <div class="col-4 d-flex flex-column justify-content-center align-items-center py-4">
                                            <h6 class="my-0"><span class="counter">50</span></h6><small>Credit Rating</small>
                                        </div>
                                        <!-- /.col-4 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.widget-user-profile -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                    <!-- Tabs Content -->
                    <div class="col-12 col-md-8 mr-b-30">
                        <ul class="nav nav-tabs contact-details-tab">
                            <li class="nav-item"><a href="#activity-tab-bordered-1" class="nav-link active" data-toggle="tab"><i class="list-icon fa fa-users"></i> Refferal Activity</a>
                            </li>
                            <li class="nav-item"><a href="#profile-tab-bordered-1" class="nav-link" data-toggle="tab"><i class="feather feather-settings "></i> Profile Settings</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="activity-tab-bordered-1">
                                <div class="widget-user-activities">
                                    <div class="single media">
                                       
                                    </div>
                                    <!-- /.single -->
                                    <div class="single media">
                                       <div class="widget-bg">
                        <div class="widget-body pb-0">
                              <h5 class="box-title"><i class="feather feather-info "></i> Your instructions on how to reffer users</h5>
                                                  <p style="font-weight: bold;">Reffering a user will allow you to get 5% of every deposit they will make into the network!</p>
                                                  <p>1.Give them this email address <span style="font-weight: bold; color: green;">{{ $user->email }}</span>.</p>
                                                  <p>2.Instruct them to enter your email as a promo refferal and they will get extra credit points.</p>
                                                   <p>3.The system will track all your subjects and you will benefit forever.</p>
                                                 
                        </div>
                        <!-- /.widget-body -->
                    </div>
                    <!-- /.widget-bg -->
                                    </div>
                                    <!-- /.single -->
                                    
                                </div>
                                <!-- /.widget-user-activities -->
                            </div>
                            <!-- /.tab-pane -->


                            <div role="tabpanel" class="tab-pane" id="profile-tab-bordered-1">
                                <div class="contact-details-profile">
                                    <h5 class="mr-b-20">Personal Profile</h5>

                                    <form action="{{route('profile.update')}}" method="post">
                                     {{ csrf_field()  }} 
                                     

                                    <div class="form-row">
                                          <div class="col">
                                           <input type="text" name="name"  class="form-control" value="{{ $user->name }}" required placeholder="First name">

                                    </div>
                                               <div class="col">
                                              <input type="text" name="surname"  class="form-control" placeholder="Last name"  value="{{ $user->surname }}" required>
                                               </div>
                                    </div>
                                     <hr>
                                    <div class="form-row">
                                         <div class="col">
                                        <input type="text" name="phone"  class="form-control" value="{{ $user->phone }}" placeholder="Contact">
                                        </div>
                                        <div class="col">
                                         <input type="text" name="email"  class="form-control" placeholder="Email" value="{{ $user->email }}" required>
                                         </div>
                                    </div>

                                    <hr>
                                    <h5 class="mr-b-20">Additional Information</h5>
                                    <div class="form-row">
                                          <div class="col">
                                           <input type="text" name="gender"  class="form-control" value="{{ $user->gender }}" placeholder="Gender">
                                           </div>
                                               <div class="col">
                                              <input type="text" name="dob"  class="form-control" value="{{ $user->dob }}"  placeholder="Date of Birth">
                                               </div>
                                    </div>
                                     <hr>
                                    <div class="form-row">
                                         <div class="col">
                                        <input type="text" name="ecocash"  class="form-control" placeholder="Ecocash Number" value="{{ $user->ecocash }}" >
                                        </div>
                                        <div class="col">
                                         <input type="text" name="location"  class="form-control" placeholder="Address" value="{{ $user->location }}" >
                                         </div>
                                    </div>
                                    <hr>
                                     @if (session()->has('impersonate'))  
                                     <div class="form-row">
                                          <div class="col-md-4">
                                           <input type="text" name="balance"  class="form-control" value="{{ $user->balance }}"  >

                                    </div>
                                    @else
                                     <input type="hidden" name="balance"  value="{{ $user->balance }}" id="balance">
                                    @endif
                                    <div class="col-lg-3 col-sm-6 col-12 mr-b-20">
                                        <button type="submit" class="btn btn-block btn-default ripple">Update Details</button>
                                    </div>
                                 </form>
                                    <hr>
                                </div>
                                <!-- /.contact-details-profile -->
                               
                                    </div>
                                    <!-- /.col-sm-6 -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.col-sm-8 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.widget-list -->
        </main>
        <!-- /.main-wrappper -->
        <!-- RIGHT SIDEBAR -->
        <aside class="right-sidebar scrollbar-enabled suppress-x">
            <div class="sidebar-chat" data-plugin="chat-sidebar">
                <div class="sidebar-chat-info">
                    <h6 class="fs-16">Chat List</h6>
                    <form class="mr-t-10">
                        <div class="form-group">
                            <input type="search" class="form-control form-control-rounded fs-13 heading-font-family pd-r-30" placeholder="Search for friends ..."> <i class="feather feather-search post-absolute pos-right vertical-center mr-3 text-muted"></i>
                        </div>
                    </form>
                </div>
                <div class="chat-list">
                    <div class="list-group row">
                        <a href="javascript:void(0)" class="list-group-item" data-chat-user="Julein Renvoye">
                            <figure class="thumb-xs user--online mr-3 mr-0-rtl ml-3-rtl">
                                <img src="assets/demo/users/user2.jpg" class="rounded-circle" alt="">
                            </figure><span><span class="name">Gene Newman</span>  <span class="username">@gene_newman</span> </span>
                        </a>
                        <a href="javascript:void(0)" class="list-group-item" data-chat-user="Eddie Lebanovkiy">
                            <figure class="thumb-xs user--online mr-3 mr-0-rtl ml-3-rtl">
                                <img src="assets/demo/users/user3.jpg" class="rounded-circle" alt="">
                            </figure><span><span class="name">Billy Black</span>  <span class="username">@billyblack</span> </span>
                        </a>
                        <a href="javascript:void(0)" class="list-group-item" data-chat-user="Cameron Moll">
                            <figure class="thumb-xs user--online mr-3 mr-0-rtl ml-3-rtl">
                                <img src="assets/demo/users/user5.jpg" class="rounded-circle" alt="">
                            </figure><span><span class="name">Herbert Diaz</span>  <span class="username">@herbert</span> </span>
                        </a>
                        <a href="javascript:void(0)" class="list-group-item" data-chat-user="Bill S Kenny">
                            <figure class="user--busy thumb-xs mr-3 mr-0-rtl ml-3-rtl">
                                <img src="assets/demo/users/user4.jpg" class="rounded-circle" alt="">
                            </figure><span><span class="name">Sylvia Harvey</span>  <span class="username">@sylvia</span> </span>
                        </a>
                        <a href="javascript:void(0)" class="list-group-item" data-chat-user="Trent Walton">
                            <figure class="user--busy thumb-xs mr-3 mr-0-rtl ml-3-rtl">
                                <img src="assets/demo/users/user6.jpg" class="rounded-circle" alt="">
                            </figure><span><span class="name">Marsha Hoffman</span>  <span class="username">@m_hoffman</span> </span>
                        </a>
                        <a href="javascript:void(0)" class="list-group-item" data-chat-user="Julien Renvoye">
                            <figure class="user--offline thumb-xs mr-3 mr-0-rtl ml-3-rtl">
                                <img src="assets/demo/users/user7.jpg" class="rounded-circle" alt="">
                            </figure><span><span class="name">Mason Grant</span>  <span class="username">@masongrant</span> </span>
                        </a>
                        <a href="javascript:void(0)" class="list-group-item" data-chat-user="Eddie Lebaovskiy">
                            <figure class="user--offline thumb-xs mr-3 mr-0-rtl ml-3-rtl">
                                <img src="assets/demo/users/user8.jpg" class="rounded-circle" alt="">
                            </figure><span><span class="name">Shelly Sullivan</span>  <span class="username">@shelly</span></span>
                        </a>
                    </div>
                    <!-- /.list-group -->
                </div>
                <!-- /.chat-list -->
            </div>
            <!-- /.sidebar-chat -->
        </aside>
        <!-- CHAT PANEL -->
        <div class="chat-panel" hidden>
            <div class="card">
                <div class="card-header d-flex justify-content-between"><a href="javascript:void(0);"><i class="feather feather-message-square text-success"></i></a>  <span class="user-name heading-font-family fw-400">John Doe</span> 
                    <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="widget-chat-activity flex-1">
                        <div class="messages scrollbar-enabled suppress-x">
                            <div class="message media reply">
                                <figure class="thumb-xs2 user--online">
                                    <a href="#">
                                        <img src="assets/demo/users/user3.jpg" class="rounded-circle" alt="">
                                    </a>
                                </figure>
                                <div class="message-body media-body">
                                    <p>Epic Cheeseburgers come in all kind of styles.</p>
                                </div>
                                <!-- /.message-body -->
                            </div>
                            <!-- /.message -->
                            <div class="message media">
                                <figure class="thumb-xs2 user--online">
                                    <a href="#">
                                        <img src="assets/demo/users/user1.jpg" class="rounded-circle" alt="">
                                    </a>
                                </figure>
                                <div class="message-body media-body">
                                    <p>Cheeseburgers make your knees weak.</p>
                                </div>
                                <!-- /.message-body -->
                            </div>
                            <!-- /.message -->
                            <div class="message media reply">
                                <figure class="thumb-xs2 user--offline">
                                    <a href="#">
                                        <img src="assets/demo/users/user5.jpg" class="rounded-circle" alt="">
                                    </a>
                                </figure>
                                <div class="message-body media-body">
                                    <p>Cheeseburgers will never let you down.</p>
                                    <p>They'll also never run around or desert you.</p>
                                </div>
                                <!-- /.message-body -->
                            </div>
                            <!-- /.message -->
                            <div class="message media">
                                <figure class="thumb-xs2 user--online">
                                    <a href="#">
                                        <img src="assets/demo/users/user1.jpg" class="rounded-circle" alt="">
                                    </a>
                                </figure>
                                <div class="message-body media-body">
                                    <p>A great cheeseburger is a gastronomical event.</p>
                                </div>
                                <!-- /.message-body -->
                            </div>
                            <!-- /.message -->
                            <div class="message media reply">
                                <figure class="thumb-xs2 user--busy">
                                    <a href="#">
                                        <img src="assets/demo/users/user6.jpg" class="rounded-circle" alt="">
                                    </a>
                                </figure>
                                <div class="message-body media-body">
                                    <p>There's a cheesy incarnation waiting for you no matter what you palete preferences are.</p>
                                </div>
                                <!-- /.message-body -->
                            </div>
                            <!-- /.message -->
                            <div class="message media">
                                <figure class="thumb-xs2 user--online">
                                    <a href="#">
                                        <img src="assets/demo/users/user1.jpg" class="rounded-circle" alt="">
                                    </a>
                                </figure>
                                <div class="message-body media-body">
                                    <p>If you are a vegan, we are sorry for you loss.</p>
                                </div>
                                <!-- /.message-body -->
                            </div>
                            <!-- /.message -->
                        </div>
                        <!-- /.messages -->
                    </div>
                    <!-- /.widget-chat-acitvity -->
                </div>
                <!-- /.card-body -->
                <form action="javascript:void(0)" class="card-footer" method="post">
                    <div class="d-flex justify-content-end"><i class="feather feather-plus-circle list-icon my-1 mr-3"></i>
                        <textarea class="border-0 flex-1" rows="1" style="resize: none" placeholder="Type your message here"></textarea>
                        <button class="btn btn-sm btn-circle bg-transparent" type="submit"><i class="feather feather-arrow-right list-icon fs-26 text-success"></i>
                        </button>
                    </div>
                </form>
            </div>
            <!-- /.card -->
        </div>
        <!-- /.chat-panel -->
        
    </div>
    <!-- /.content-wrapper -->
 
@endsection