@extends('layouts.v2applist')

@section('content')

 <main class="pattern">
       @include('includes.v2banner')
     
         <span><em></em></span>
        <div  class="container margin_60_35">
            <div class="main_title_3">
                <span></span>
                <h2 style="margin-bottom:2.5vw;">Featured Restaurants</h2>
               
                <a href="{{ url('/zim/categories/food-drinks-restaurants/listings') }}">See all <i class="fas fa-arrow-right"></i></a>
            </div>
            <div class="row add_bottom_30">
                  @foreach ($restaurants as $restaurant)
                        @include ('listings.partials.v2base_res', compact('restaurant'))
                     @endforeach
            </div>
            <!-- /row -->
            
            @include('includes.v2areas')

            <div class="main_title_3">
                <span></span>
                <h2 style="margin-bottom:2.5vw;">Featured lodges</h2>
               
                <a href="{{ url('/zim/categories/hotels-recreation-lodges/listings') }}">See all <i class="fas fa-arrow-right"></i></a>
            </div>
            <div class="row add_bottom_30">
              
                     @foreach ($listings as $listing)
                        @include ('listings.partials.v2base_lodges', compact('listing'))
                     @endforeach
              
                
            </div>
            <!-- /row -->
              
            <div class="main_title_3">
                <span></span>
                <h2 style="margin-bottom:2.5vw;">Popular Universities</h2>
                
                <a href="{{ url('/zim/categories/education-universities/listings') }}">See all <i class="fas fa-arrow-right"></i></a>
            </div>
            <div class="row add_bottom_30">
                 @foreach ($schools as $school)
                        @include ('listings.partials.v2base_var', compact('school'))
                     @endforeach
            </div>
            <!-- /row -->
            
            
        </div>
        <!-- /container -->
        
        <div class="call_section">
            <div class="wrapper">
                <div class="container margin_80_55">
                    <div class="main_title_2">
                        <span><em></em></span>
                        <h2>How Ndepapi Works</h2>
                        <p>We allow you to profile your business in 3 simple steps.</p>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="box_how">
                                <i class="fas fa-folder-plus"></i>
                                <h3>Create an Account</h3>
                                <p>Create an account using the email address you want to recieve notifications on.</p>
                                <span></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="box_how">
                               <i class="fas fa-paste"></i>
                                <h3>Create a Listing</h3>
                                <p>Using your account, create a listing in the exact area you want to be seen and involve quality images.</p>
                                <span></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="box_how">
                               <i class="fas fa-headset"></i>
                                <h3>Reply emails and Calls</h3>
                                <p>Done with the second step? Congrats, your customers will start to flock your inbox and you become profitable.</p>
                            </div>
                        </div>
                    </div>
                    <!-- /row -->
                    <p class="text-center add_top_30 wow bounceIn" data-wow-delay="0.5s"><a href="{{ url('/register') }}" class="btn_1 rounded">Create Account</a></p>
                </div>
                <canvas id="hero-canvas" width="1920" height="480"></canvas>
            </div>
            <!-- /wrapper -->
        </div>
        <!--/call_section-->
        
       
        <!-- /container -->
    </main>
    <!-- /main -->

 
 
@endsection