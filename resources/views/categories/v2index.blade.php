@extends('layouts.v2applist')

@section('content')
     
 @include('includes.v2banner')

<div class="bg_color_1">
            <div class="container margin_80_55">
                <div class="main_title_2">
                    <span><em></em></span>
                    <h2>All categories in {{ $area->name }}</h2>
                    <p>Expand any parent category and view all sub categories.</p>
                </div>
                <div class="row justify-content-center">
                    @foreach ($categories as $category)
                    <div class="col-lg-3 col-md-6">
                        <div class="box_cat_home">
                          
                            <i class="fas {{ $category->icon }}" style="color:#{{ $category->color }};"></i>
                            <h3>{{ $category->name }}</h3>
                            @foreach ($category->children as $sub)
                            <ul class="clearfix">
                                <li><strong><a href="{{ route('listings.display.index3', [$area, $sub]) }}"><font color="#555;">{{ $sub->name }}</font></a></strong></li>
                            </ul>
                            @endforeach
                        </div>
                    </div>
                    @endforeach
                </div>
                <!-- /row -->
            </div>
            <!-- /container --> 

        </div>
        <!-- /bg_color_1 -->
        
            
@endsection
