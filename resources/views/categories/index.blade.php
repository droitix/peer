@extends('layouts.app2')

@section('content')
     
 @include('includes.banner')

        
      <div class="section services">
                 @foreach ($categories as $category)
                <!-- single-service -->
                <div class="single-service">
                    
                    
                     <div class="services-icon">
                        <i class="fas {{ $category->icon }} fa-3x" style="color:#{{ $category->color }};"></i>
                     </div>
                    <h5 style="text-transform: uppercase; font-size: 14px;">{{ $category->name }}</h5>
                   
                        @foreach ($category->children as $sub)
                         <ul>
                        <li style="text-transform: uppercase; font-size: 13px;"><a href="{{ route('listings.display.index', [$area, $sub]) }}">{{ $sub->name }}</a></li>
                       
                    </ul>
                      @endforeach
                
                </div><!-- single-service -->   
                     @endforeach
               
                    
            </div><!-- services -->         
@endsection
