
    <!-- Basic metas
    ======================================== -->
    <meta charset="utf-8">
    <meta name="author" content="Axilweb">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!-- Mobile specific metas
    ======================================== -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Page Title
    ======================================== -->
    <title>Kryptova</title>

    <!-- links for favicon
    ======================================== -->
    <link rel="shortcut icon" href="icon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="icon/favicon.ico" type="image/x-icon">

    <!-- Icon fonts
    ======================================== -->
    <link rel="stylesheet" type="text/css" href="css/cryptowallet-argon.css">

    <!-- css links
    ======================================== -->
    <!-- Bootstrap link -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css">

    <!-- Magnific popup -->
    <link rel="stylesheet" type="text/css" href="css/magnific-popup.css">

    <!-- Custom styles -->
    <link rel="stylesheet" type="text/css" href="css/main.css">

    <!-- Responsive styling -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">