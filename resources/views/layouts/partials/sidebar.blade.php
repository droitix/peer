
        <!-- SIDEBAR -->
        <aside class="site-sidebar scrollbar-enabled" data-suppress-scroll-x="true">
            <!-- User Details -->
            <div class="side-user d-none">
                <div class="col-sm-12 text-center p-0 clearfix">
                    <div class="d-inline-block pos-relative mr-b-10">
                        <figure class="thumb-sm mr-b-0 user--online">
                            <img src="/assets/demo/users/user1.jpg" class="rounded-circle" alt="">
                        </figure><a href="page-profile.html" class="text-muted side-user-link"><i class="feather feather-settings list-icon"></i></a>
                    </div>
                    <!-- /.d-inline-block -->
                    <div class="lh-14 mr-t-5"><a href="page-profile.html" class="hide-menu mt-3 mb-0 side-user-heading fw-500">Scott Adams</a>
                        <br><small class="hide-menu">Developer</small>
                    </div>
                </div>
                <!-- /.col-sm-12 -->
            </div>
            <!-- /.side-user -->
            <!-- Call to Action -->
            <div class="side-content mr-t-30 mr-lr-15"><a class="btn btn-block btn-dark ripple fw-600" href="{{ route('listings.create', [$area]) }}"><i class="fa fa-plus-square-o mr-1 mr-0-rtl ml-1-rtl"></i> Buy Coins</a>
            </div>
            <!-- Sidebar Menu -->
            <nav class="sidebar-nav">
                <ul class="nav in side-menu">

                    <li class="current-page menu-item-has-children"><a href="{{ url('/dashboard') }}"><i class="list-icon feather feather-layers"></i><span class="hide-menu">Dashboard</span</a>

                    </li>


                    <li class="menu-item-has-children"><a href="{{ url('/profile') }}"><i class="list-icon feather feather-user"></i> <span class="hide-menu">My Profile</span></a>


                    </li>

                    <li class="menu-item-has-children"><a href="{{ url('/transactions') }}"><i class="list-icon feather feather-folder"></i> <span class="hide-menu">My Transactions</span></a>


                    </li>
                    <li class="menu-item-has-children"><a href="{{ route('listings.create', [$area]) }}"><i class="list-icon feather feather-plus-circle"></i> <span class="hide-menu">Buy Coins</span></a>

                    </li>

                    <li class="menu-item-has-children"><a href="{{ route('listings.withdraw', [$area]) }}"><i class="list-icon feather feather-refresh-cw"></i> <span class="hide-menu">Withdraw Funds</a>

                    </li>


                    <li class="menu-item-has-children"><a href="{{ url('/profile') }}"><i class="list-icon feather feather-zap"></i> <span class="hide-menu">Refferal Plan</a>

                    </li>
                     @if (auth()->user()->hasRole('admin'))
                    <li class="menu-item-has-children"><a href="{{ route('listings.display.index', [$area]) }}"><i class="list-icon feather feather-layout"></i> <span class="hide-menu">Admin Approve</span></a>

                    </li>


                    <li class="menu-item-has-children"><a href="{{ route('admin.impersonate') }}"><i class="list-icon feather feather-radio"></i> <span class="hide-menu">Impersonate</span></a>

                    </li>
                    @endif
                    @if (session()->has('impersonate'))
                    <li class="menu-item-has-children"><a href="#" onclick="event.preventDefault(); document.getElementById('impersonating').submit();"><i class="list-icon feather feather-radio"></i> <span class="hide-menu">Stop Imprsntng</span></a></li>

                    <form action="{{ route('admin.impersonate') }}" method="POST" id="impersonating" class="hidden">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    </form>
                    @endif

                </ul>
                <!-- /.side-menu -->
            </nav>
            <!-- /.sidebar-nav -->
        </aside>
        <!-- /.site-sidebar -->
