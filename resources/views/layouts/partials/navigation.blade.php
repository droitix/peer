<!-- HEADER & TOP NAVIGATION -->
        <nav class="navbar">
            <!-- Logo Area -->
            <div class="navbar-header">
                <a href="{{ url('/dashboard') }}" class="navbar-brand">
                    <img class="logo-expand" style="width: 150px;" alt="" src="/images/whitelogo.png">
                    <img class="logo-collapse" style="width: 25px; height: 30px;" alt="" src="/images/whiterlogo.png">
                    <!-- <p>BonVue</p> -->
                </a>
            </div>
            <!-- /.navbar-header -->
            <!-- Left Menu & Sidebar Toggle -->
            <ul class="nav navbar-nav">
                <li class="sidebar-toggle dropdown"><a href="javascript:void(0)" class="ripple"><i class="feather feather-menu list-icon fs-20"></i></a>
                </li>
            </ul>
            <!-- /.navbar-left -->
        
            <div class="spacer"></div>
            @guest
            <!-- Right Menu -->
            <ul class="nav navbar-nav d-none d-lg-flex ml-2 ml-0-rtl">
                <li class="dropdown"><a href="{{ url('login') }}"><i class="feather feather-unlock list-icon"></i>&nbsp Login</a>
                    
                </li>
                
                <li><a href="javascript:void(0);" class="right-sidebar-toggle"><i class="feather feather-user-plus list-icon"></i>&nbsp Signup</a>
                </li>
            </ul>
            <!-- /.navbar-right -->
           
    <!-- /.navbar-nav -->

            @else
            <!-- Right Menu -->
            <ul class="nav navbar-nav d-none d-lg-flex ml-2 ml-0-rtl">
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="feather feather-bell list-icon"></i> <span class="button-pulse bg-danger"></span></a>
                    <div class="dropdown-menu dropdown-left dropdown-card animated flipInY">
                        <div class="card">
                            <header class="card-header d-flex justify-content-between mb-0"><a href="javascript:void(0);"><i class="feather feather-bell color-primary" aria-hidden="true"></i></a>  <span class="heading-font-family flex-1 text-center fw-400">Notifications</span>  <a href="javascript:void(0);"><i class="feather feather-settings color-content"></i></a>
                            </header>
                            <ul class="card-body list-unstyled dropdown-list-group">
                                <li><a href="#" class="media"><span class="d-flex"><i class="material-icons list-icon">check</i> </span><span class="media-body"><span class="heading-font-family media-heading">Welcome to streetwage</span> <span class="media-content">Thank you for joining streetwage enjoy the network...</span></span></a>
                                </li>
                               
                               
                            </ul>
                            <!-- /.dropdown-list-group -->
                            <footer class="card-footer text-center"><a href="{{ url('profile') }}" class="headings-font-family text-uppercase fs-13">Update your profile</a>
                            </footer>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.dropdown-menu -->
                </li>
                <!-- /.dropdown -->
              
            </ul>
            <!-- /.navbar-right -->
            <!-- User Image with Dropdown -->
            <ul class="nav navbar-nav">
                <li><a href="{{ url('profile') }}">Hi, {{ Auth::user()->name }}</a></li>
                <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle dropdown-toggle-user ripple" data-toggle="dropdown"><span class="avatar thumb-xs2"><img src="/uploads/avatars/{{ Auth::user()->avatar }}" class="rounded-circle" alt=""> <i class="feather feather-chevron-down list-icon"></i></span></a>
                    <div
                    class="dropdown-menu dropdown-left dropdown-card dropdown-card-profile animated flipInY">
                        <div class="card">
                            <header class="card-header d-flex mb-0"><a href="javascript:void(0);" class="col-md-4 text-center"><i class="feather feather-user-plus align-middle"></i> </a><a href="javascript:void(0);" class="col-md-4 text-center"><i class="feather feather-settings align-middle"></i> </a>
                                <a
                                href="javascript:void(0);" class="col-md-4 text-center"><i class="feather feather-power align-middle"></i>
                                    </a>
                            </header>
                            <ul class="list-unstyled card-body">
                                <li><a href="#"><span><span class="align-middle">Account Balance  ${{ Auth::user()->balance }}.00</span></span></a>
                                </li>
                                <li><a href="{{ route('profile') }}"><span><span class="align-middle">My Profile</span></span></a>
                                </li>
                                <li><a href="{{ route('listings.create', [$area]) }}"><span><span class="align-middle">Make Deposit</span></span></a>
                                </li>
                                 <li><a href="{{ route('listings.withdraw', [$area]) }}"><span><span class="align-middle">Withdraw Funds</span></span></a>
                                </li>
                                <li><a href="{{ url('/transactions') }}"><span><span class="align-middle">My Transactions</span></span></a>
                                </li>
                                <li><a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();"><span><span class="align-middle">Sign Out</span></span></a>
                                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                       @csrf
                                                       </form>
                                </li>
                            </ul>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
    </div>
    <!-- /.dropdown-card-profile -->
    </li>
    <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-nav -->
    @endguest
    </nav>
    <!-- /.navbar -->