<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
   @include('layouts.partials.homehead')
 
</head>
<body class="sidebar-dark sidebar-expand navbar-brand-dark header-light">
    <div id="wrapper" class="wrapper">
   @include('layouts.partials.navigation')
       <div class="content-wrapper">


            @yield('content')
         

         
        </div>

       

    </div>

       
   <!-- Javascripts
    ======================================= -->
    <!-- jQuery -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/jquery-migrate.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap.bundle.min.js"></script>
    <!-- font awesome -->
    <script src="js/fontawesome-all.min.js"></script>
    <!-- jQuery Countdown plugin -->
    <script src="js/jquery.countdown.min.js"></script>
    <!-- jQuery Easing Plugin -->
    <script src="js/easing-1.3.js"></script>
    <!-- jQuery progressbar plugin -->
    <script src="js/jquery.waypoints.min.js"></script>
    <!-- Bootstrap Progressbar -->
    <script src="js/bootstrap-progressbar.min.js"></script>
    <!-- ImagesLoaded js -->
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <!-- Slick carousel js -->
    <script src="js/slick.min.js"></script>
    <!-- Magnific popup -->
    <script src="js/jquery.magnific-popup.min.js"></script>

    <!-- pie chart -->
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <!-- Custom js -->
    <script src="js/main.js"></script>
   
</body>
</html>
