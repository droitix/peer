<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'Short Term',
                
                'children' => [
                    ['name' => '14 days'],
                    ['name' => '1 month'],
                    
                    
                ]
            ],
            [
                'name' => 'Medium High Return',
               
                'children' => [
                    ['name' => '2 months'],
                    ['name' => '3 months'],
                    
                    
                ]
            ],
            [
                'name' => 'Long Term',
               
                'children' => [
                    ['name' => '5 months'],
                    ['name' => '1 year'],
                    
                  ]
            ],
            
           
        ];

        foreach ($categories as $category) {
            \App\Category::create($category);
        }
    }
}
