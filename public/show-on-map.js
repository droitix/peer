$(document).ready(function(){

  var generateRandomColor = function() {
      var letters = '0123456789ABCDEF';
      var color = '';
      for(var i = 0; i < 6; i++){
          color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
  }

  let initialize  = function(address, placeName, others) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function(results, status) {
      
      if (status == google.maps.GeocoderStatus.OK) {
        let Lat = results[0].geometry.location.lat();
        let Lng = results[0].geometry.location.lng();
        let mapCenter = new google.maps.LatLng(Lat, Lng);
        let infowindow =  new google.maps.InfoWindow({});

        let map = new google.maps.Map(document.getElementById("map_canvas"), {
          zoom: 12,
          center: mapCenter
        });

        let marker = new google.maps.Marker({
          position: mapCenter,
          map: map,
          title: placeName + "\n" + address
        });

          google.maps.event.addListener(marker, 'mouseover', (function(marker, count) {
              return function() {
                  infowindow.setContent(placeName + "\n" + address);
                  infowindow.open(map, marker);
              }
          })(marker));

          google.maps.event.addListener(marker, 'click', (function(marker, count) {
              return function() {
                  infowindow.setContent(placeName + "\n" + address);
                  infowindow.open(map, marker);
                  map.panTo(this.getPosition());
              }
          })(marker));

        for (let i = 0; i < others.length; i++) {
          var icon = 'https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|' + generateRandomColor();

          geocoder.geocode({'address': others[i].address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              marker = new google.maps.Marker({
                  position: new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng()),
                  map: map,
                  icon: icon,
                  title: others[i].title + "\n" + others[i].address,
                  optimized: false
              });

              google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
                  return function() {
                      infowindow.setContent(others[i].title + "\n" + others[i].address);
                      infowindow.open(map, marker);
                  }
              })(marker, i));

              google.maps.event.addListener(marker, 'click', (function(marker, i) {
                  return function() {
                      infowindow.setContent(others[i].title + "\n" + others[i].address);
                      infowindow.open(map, marker);
                      map.panTo(this.getPosition());
                  }
              })(marker, i));
            }
          });
        }

      } else {
        console.error("Something went wrong", status);
      }
    });
  }

  $('#show-address').on('click', function(e){
    let address = e.target.dataset.address;
    let placeName = e.target.dataset.placeName;

    getOthersInSameCategory(
      e.target.dataset.address,
      e.target.dataset.placeName,
      e.target.dataset.area,
      e.target.dataset.category
    );
    
  });

  function getOthersInSameCategory(address, placeName, area, category){
    $.ajax({
      url: `/${area}/listings/same-category/${category}`,
      type: "GET",
      data: { 
        _token: document.getElementsByName("_token").value
      },
      async: false,
      success: function(response) {
        if(response.success){
          return initialize(address, placeName, response.other_places);
        }
        return initialize(address, placeName, null);
      },
      error: function(xhr) {
        return initialize(address, placeName, null);
      }
    });
  }

});

