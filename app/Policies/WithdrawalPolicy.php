<?php

namespace App\Policies;

use App\{User, Withdrawal};
use Illuminate\Auth\Access\HandlesAuthorization;

class DepositPolicy
{
    use HandlesAuthorization;

    public function edit(User $user, Withdrawal $withdrawal)
    {
        return $this->touch($user, $withdrawal);
    }

    public function update(User $user, Withdrawal $withdrawal)
    {
        return $this->touch($user, $withdrawal);
    }

    public function destroy(User $user, Withdrawal $withdrawal)
    {
        return $this->touch($user, $withdrawal);
    }

    public function touch(User $user, Withdrawal $withdrawal)
    {
        return $withdrawal->ownedByUser($user);
    }
}
