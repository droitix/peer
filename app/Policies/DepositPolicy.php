<?php

namespace App\Policies;

use App\{User, Deposit};
use Illuminate\Auth\Access\HandlesAuthorization;

class DepositPolicy
{
    use HandlesAuthorization;

    public function edit(User $user, Deposit $deposit)
    {
        return $this->touch($user, $deposit);
    }

    public function update(User $user, Deposit $deposit)
    {
        return $this->touch($user, $deposit);
    }

    public function destroy(User $user, Deposit $deposit)
    {
        return $this->touch($user, $deposit);
    }

    public function touch(User $user, Deposit $deposit)
    {
        return $deposit->ownedByUser($user);
    }
}
