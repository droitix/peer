<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Image;

class UserController extends Controller
{
    public function profile(Request $request)
  {
      $user=Auth::user();
      $listings = $request->user()->listings()->latestFirst()->paginate(10);

    return view('profile.profile', compact('listings','user'));
  }

   public function transactions(Request $request)
  {
      $user=Auth::user();
      $listings = $request->user()->listings()->latestFirst()->paginate(10);

    return view('profile.transactions', compact('listings','user'));
  }

   public function update_avatar(Request $request)
  {
      if($request->hasFile('avatar')){
        $avatar = $request->file('avatar');
        $filename = time() . '.' . $avatar->getClientOriginalExtension();
        Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/avatars/' .$filename) );

        $user = Auth::user();
        $user->avatar = $filename;
        $user->save();
      }
      return redirect()->back()->withInput();
  }
    public function update(Request $request)
  {  
    $request = $request->validate([
      'name' => 'required',
      'email' => 'required',
      'surname' => 'required',
      'balance' => 'required',
      'phone' => '',
      'ecocash' => '',
      'gender' => '',
      'dob' => '',
      'location' => '',

     
     
    ]);

    $saved = false;
    $user = auth()->user();
    $user->name = $request['name'];
    $user->surname = $request['surname'];
    $user->phone = $request['phone'];
    $user->email = $request['email'];
    $user->ecocash = $request['ecocash'];
    $user->gender = $request['gender'];
    $user->dob = $request['dob'];
    $user->location = $request['location'];
    $user->balance = $request['balance'];


    if($user->save()) $saved = true;

    return view('profile.profile', compact('saved', 'user'));
  }

}
