<?php

namespace App\Http\Controllers\Listing;

use App\{Area, Category, Listing};
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CommentResource;

class ListingCommentController extends Controller
{
     public function index(Request $request, Area $area, Listing $listing)
    {
       return CommentResource::collection($listing->comments()->with(['children','user'])->get());
    }

    public function store(Request $request, Area $area, Listing $listing)

    {
      

        $this->validate($request, [
            'body' => 'required|max:5000'
        ]);

        $comment = $listing->comments()->make([
            'body' => $request->body
        ]);

        $request->user()->comments()->save($comment);

         return new CommentResource($comment);
  
    }
}
