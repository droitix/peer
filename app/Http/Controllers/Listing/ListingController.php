<?php

namespace App\Http\Controllers\Listing;

use App\{Area, Category, Listing};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Jobs\UserViewedListing;
use App\Http\Requests\StoreListingFormRequest;

class ListingController extends Controller
{
    public function index(Area $area, Category $category)
    {
        $listings = Listing::with(['user', 'area'])->isNotLive()->latestFirst()->paginate(15);



        return view('listings.display.index', compact('listings', 'category'));

    }

  

    public function show(Request $request, Area $area, Listing $listing)
    {
        if (!$listing->live()) {
            abort(404);
        }

        if ($request->user()) {
            dispatch(new UserViewedListing($request->user(), $listing));
        }

        return view('listings.show', compact('listing'));
    }

    public function create()
    {
        return view('listings.create');
    }

    public function withdraw()
    {
        return view('listings.withdraw');
    }

    public function store(StoreListingFormRequest $request, Area $area)
    {
        $listing = new Listing;
        $listing->amount = $request->amount;
        $listing->code = $request->code;
        $listing->state = $request->state;
        $listing->fullname = $request->fullname;
        $listing->category_id = $request->category_id;
        $listing->area_id = $request->area_id;
        $listing->user()->associate($request->user());
         
       

        $this->authorize('touch', $listing);

        if ($listing->cost() > 0) {
            return back();
        }

        $listing->live = false;
        $listing->created_at = \Carbon\Carbon::now();
        $listing->save();

        return redirect()
            ->route('profile', [$area, $listing])
            ->withSuccess('Congrats!!.Your deposit slip is being approved');
    }

    public function edit(Request $request, Area $area, Listing $listing)
    {
        $this->authorize('edit', $listing);
        
        return view('listings.edit', compact('listing'));
    }

    public function update(StoreListingFormRequest $request, Area $area, Listing $listing)
    {
        $this->authorize('update', $listing);

        $listing->amount = $request->amount;
        $listing->fullname = $request->fullname;
        $listing->code = $request->code;
        $listing->live = $request->live;
        $listing->declined = $request->declined;
        $listing->message = $request->message;
  



      
        if (!$listing->live()) {
            $listing->category_id = $request->category_id;
        }

        $listing->area_id = $request->area_id;

        
        

        $listing->save();

               
        return back()->withSuccess('Admin decision registered.');
    }

    public function destroy(Area $area, Listing $listing)
    {
        $this->authorize('destroy', $listing);

        $listing->delete();

        return back()->withSuccess('Listing was deleted.');
    }
 /**
     * Search for listings by category and keywords.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request, Area $area)
    {

        
        $keyword = $request->get('query');

    
        if(empty($keyword))
        {
            //we'll return an empty listing collection if no category or keyword
            $listings = collect(new Listing);
            return view('search')->with(compact('listings'));
        }


      


        

        $listings = Listing::SearchByKeyword($keyword)->paginate(20);

        return view('search')->with(compact('listings'));
    }

}