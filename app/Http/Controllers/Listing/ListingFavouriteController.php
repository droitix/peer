<?php

namespace App\Http\Controllers\Listing;

use App\{Area, Listing};
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ListingFavouriteController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(Request $request)
    {
        $user = Auth::user();

        $listings = $request->user()->favouriteListings()->with(['user', 'area'])->paginate(5);

        return view('user.listings.favourites.v2index', compact('listings','user'));
    }

    public function store(Request $request, Area $area, Listing $listing)
    {
        $request->user()->favouriteListings()->syncWithoutDetaching([$listing->id]);

       notify()->flash('Listing added to favourites', 'success', [
    'timer' => 1000,
    'text' => 'View your saved listings',
]);

        return back();
    }

    public function destroy(Request $request, Area $area, Listing $listing)
    {
        $request->user()->favouriteListings()->detach($listing);

        return back()->withSuccess('Listing removed to favourites.');
    }
}

