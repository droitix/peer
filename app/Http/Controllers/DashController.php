<?php

namespace App\Http\Controllers;

use App\{Area, Category, Listing};
use Illuminate\Http\Request;

class DashController extends Controller
{
      const INDEX_LIMIT = 10;

    public function dashboard(Area $area, Category $category, Listing $listing)
    {
        $areas = Area::get()->toTree();

       $listings = Listing::with(['user'])->isLive()->latestFirst()->paginate(5);

        return view('dashboard', compact('areas','listings'));
    }

}
