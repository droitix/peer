<?php

namespace App\Http\Controllers;

use App\{Area, Category, Listing};
use Illuminate\Http\Request;

class HomeController extends Controller
{
      const INDEX_LIMIT = 10;

    public function index(Area $area, Category $category, Listing $listing)
    {
        $areas = Area::get()->toTree();

       $listings = Listing::with(['user'])->isLive()->latestFirst()->paginate(10);

        return view('home', compact('areas','listings'));
    }

}
